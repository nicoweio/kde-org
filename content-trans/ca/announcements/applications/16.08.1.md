---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE distribueix les aplicacions 16.08.1 del KDE
layout: application
title: KDE distribueix les aplicacions 16.08.1 del KDE
version: 16.08.1
---
8 de setembre de 2016. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../16.08.0'>aplicacions 16.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 45 esmenes registrades d'errors que inclouen millores al Kdepim, Kate, Kdenlive, Konsole, Marble, Kajongg, Kopete, i Umbrello, entre altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.24.
