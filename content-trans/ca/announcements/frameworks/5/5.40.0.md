---
aliases:
- ../../kde-frameworks-5.40.0
date: 2017-11-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Considera els fitxers DjVu com a documents (error 369195)
- Esmena l'ortografia de manera que les presentacions WPS de l'Office es reconeguin correctament

### Icones Brisa

- Afegeix la carpeta de modificacions temporals per a la icona de modificacions temporals de la barra d'eines del Dolphin

### KArchive

- Soluciona una fuita de memòria potencial. Esmena la lògica

### KCMUtils

- Sense marges per als mòduls QML des de la banda del «qwidget»
- Inicialitza les variables (trobat pel Coverity)

### KConfigWidgets

- Esmena la icona de KStandardAction::MoveToTrash

### KCoreAddons

- Soluciona la detecció d'URL amb URL dobles com «http://www.foo.bar&lt;http://foo.bar/&gt;»
- Usa «https» per als URL de KDE

### Compatibilitat amb les KDELibs 4

- Documentació completa per a la substitució de «disableSessionManagement()»
- Fa que «kssl» compili amb l'OpenSSL 1.1.0 (error 370223)

### KFileMetaData

- Soluciona el nom de la pantalla de la propietat Generator

### KGlobalAccel

- KGlobalAccel: Soluciona el funcionament de les tecles del teclat numèric (un altre cop)

### KInit

- Instal·lació correcta de «start_kdeinit» quan DESTDIR i la «libcap» s'usen conjuntament

### KIO

- Soluciona la visualització de «remote:/» al «qfiledialog»
- Implementa el funcionament de les categories al KfilesPlacesView
- HTTP: Esmena la cadena d'error per al cas 207 multiestat
- KNewFileMenu: Neteja del codi mort, descobert pel Coverity
- IKWS: Esmena un possible bucle infinit, descobert pel Coverity
- Funció KIO::PreviewJob::defaultPlugins()

### Kirigami

- Sintaxi que funcionava a les antigues Qt 5.7 (error 385785)
- Apila de manera diferent l'«OverlaySheet» (error 386470)
- També mostra la propietat delegada de ressaltat quan no hi ha focus
- Consells de mida preferida per al separador
- Ús correcte de Settings.isMobile
- Permet que les aplicacions siguin més convergents en un sistema «desktop-y»
- Assegura que el contingut de SwipeListItem no superposa la nansa (error 385974)
- La vista desplaçable de l'«OverlaySheet» sempre és interactiva
- Afegeix categories al fitxer «desktop» de les galeries (error 385430)
- Actualitza el fitxer «kirigami.pri»
- Usa el connector no instal·lat per fer les proves
- Fa obsolet Kirigami.Label
- Adapta l'ús de Labels d'exemple de galeria per a ser consistent amb el QQC2
- Adapta els usos de Kirigami.Controls des de Kirigami.Label
- Fa interactiva l'àrea de desplaçament en esdeveniments tàctils
- Mou la crida «git find_package» a on s'utilitza
- Els elements de «listview» són transparents de manera predefinida

### KNewStuff

- Elimina PreferCache des de les peticions de xarxa
- No desacobla apuntadors compartits a dades privades en configurar vistes prèvies
- KMoreTools: Actualitza i esmena fitxers «desktop» (error 369646)

### KNotification

- Elimina la comprovació de servidors SNI en triar si usar el mode antic (error 385867)
- Només comprova si hi ha icones antigues a la safata del sistema si se'n posarà una (error 385371)

### Framework del KPackage

- Usa els fitxers de serveis no instal·lats

### KService

- Inicialitza valors
- Inicialitza diversos apuntadors

### KTextEditor

- API dox: Esmena noms incorrectes de mètodes i arguments, afegeix un «\\since» que manca
- Evita (certes) fallades en executar scripts QML (error 385413)
- Evita una fallada del QML activada pels scripts de sagnat d'estil C
- Augmenta la mida de la marca final
- Esmena diversos indentadors per no fer el sagnat en caràcters aleatoris
- Esmena un avís d'obsolescència

### KTextWidgets

- Inicialitza un valor

### KWayland

- [client] Elimina les comprovacions que «platformName» sigui «wayland»
- No duplica la connexió a «wl_display_flush»
- Protocol estranger del Wayland

### KWidgetsAddons

- Esmena una incoherència de focus al giny «createKMessageBox»
- Un diàleg més compacte de contrasenya (error 381231)
- Defineix adequadament l'amplada de KPageListView

### KWindowSystem

- KKeyServer: Esmena la gestió de Meta+Maj+Impr, Alt+Maj+fletxes, etc.
- Implementació la plataforma «flatpak»
- Usa l'API pròpia de detecció de plataforma del KWindowSystem en lloc de codi duplicat

### KXMLGUI

- Usa «https» per als URL de KDE

### NetworkManagerQt

- 8021xSetting: El «domain-suffix-match» es defineix en el NM 1.2.0 i posteriors
- Implementa «domain-suffix-match» a Security8021xSetting

### Frameworks del Plasma

- Dibuixa manualment l'arc del cercle
- [Menú de PlasmaComponents] Afegeix «ungrabMouseHack»
- [FrameSvg] Optimitza «updateSizes»
- No posiciona el diàleg si és del tipus OSD

### QQC2StyleBridge

- Millora la compilació com a connector estàtic
- Fer el botó d'opció un botó d'opció
- Usa el «qstyle» per pintar el Dial
- Usa una ColumnLayout per als menús
- Esmena el diàleg
- Elimina la propietat de grup no vàlida
- Soluciona el format del fitxer «md» per tal de coincidir amb els altres mòduls
- Comportament del quadre combinat més proper a QQC1
- Solució temporal per als QQuickWidgets

### Sonnet

- Afegeix el mètode «assignByDictionnary»
- Assenyala si és capaç d'assignar un diccionari

### Ressaltat de la sintaxi

- Makefile: Esmena una «regexpr» que coincideix a «CXXFLAGS+»

### ThreadWeaver

- Neteja del CMake: No posar en el codi font -std=c++0x

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
