---
aliases:
- ../../kde-frameworks-5.54.0
date: 2019-01-12
layout: framework
libCount: 70
---
### Attica

- Notifica si un proveïdor predeterminat falla en descarregar

### Baloo

- Mou l'ajudant «typesForMimeType» del BasicIndexingJob a un espai de noms anònim
- Afegeix «image/svg» com a Type::Image al BasicIndexingJob
- Usa un format compacte del JSON per emmagatzemar metadades del document

### Icones Brisa

- Canvia «preferences-system-network» a un enllaç simbòlic
- Usa una icona del Kolf amb un fons agradable i ombres
- Copia diverses icones canviades des del Brisa al Brisa fosca
- Afegeix icones del YaST i de preferències noves
- Afegeix una icona «python-bytecode» adequada, usa un color coherent a les icones del Python (error 381051)
- Elimina els enllaços simbòlics de bytecode del Python per preparar-los a què esdevinguin les seves pròpies icones
- Fa que la icona de tipus MIME del Python sigui la base, i la icona de bytecode del Python un enllaç al primer
- Afegeix icones de dispositiu per als ports RJ11 i RJ45
- Afegeix un separador de directori que manca (error 401836)
- Usa la icona correcta per als scripts en Python 3 (error 402367)
- Canvia les icones de color de xarxa/web a un estil coherent
- Afegeix un nom nou per als fitxers SQLite, de manera que les icones els mostrin realment (error 402095)
- Afegeix icones «drive-*» per al particionador del YaST
- Afegeix la icona «view-private» (error 401646)
- Afegeix icones d'acció de llum de flaix
- Millora el simbolisme per a les icones d'estat apagat i silenciat (error 398975)

### Mòduls extres del CMake

- Afegeix un mòdul de cerca per al «libphonenumber» de Google

### KActivities

- Esmena la versió al fitxer «pkgconfig»

### Eines de Doxygen del KDE

- Soluciona la renderització «doxygen» del Markdown

### KConfig

- Escapa els bytes que siguin més grans o iguals a 127 als fitxers de configuració

### KCoreAddons

- Macros del CMake: Elimina l'adaptació obsoleta de les variables ECM al «kcoreaddons_add_plugin» (error 401888)
- Fa traduïbles les unitats i prefixos de «formatValue»

### KDeclarative

- No mostra els separadors en el mòbil
- «root.contentItem» en lloc de només «contentItem»
- Afegeix l'API que manca per als KCM multinivell, per a controlar les columnes

### KFileMetaData

- La prova d'escriptura falla si el tipus MIME no és admès per l'extractor
- Soluciona l'extracció del número de disc APE
- Implementa l'extracció de la caràtula als fitxers «asf»
- Amplia la llista de tipus MIME acceptats per als extractors d'imatge incrustats
- Refactoritza l'extractor d'imatges incrustades per a una ampliabilitat més gran
- Afegeix el tipus MIME «wav» que mancava
- Extreu més etiquetes de les metadades EXIF (error 341162)
- Soluciona l'extracció de l'altitud GPS de la dada de l'EXIF

### KGlobalAccel

- Soluciona la construcció de KGlobalAccel amb el prellançament de les Qt 5.13

### KHolidays

- README.md - Afegeix instruccions bàsiques per provar els fitxers de festius de proves
- Diversos calendaris - esmena errors de sintaxi

### KIconThemes

- ksvg2icns: usa l'API QTemporaryDir de les Qt 5.9+

### KIdleTime

- [xscreensaverpoller] Neteja després de reiniciar l'estalvi de pantalla

### KInit

- Usa el «rlimit» suau per numerar els gestors oberts. Això soluciona un inici molt lent del Plasma amb el darrer «systemd».

### KIO

- Reverteix «Oculta la vista prèvia del fitxer quan la icona és massa petita»
- Mostra un error en lloc de fallar en silenci quan es demana crear una carpeta que ja existeix (error 400423)
- Canvia el camí de cada element dels subdirectoris en reanomenar un directori (error 401552)
- Amplia el filtratge de l'expressió regular de «getExtensionFromPatternList»
- [KRun] En demanar d'obrir un enllaç a un navegador extern, estableix «mimeapps.list» com a reserva si no s'ha definit res a «kdeglobals» (error 100016)
- Fa que la funcionalitat obre l'URL en una pestanya sigui una mica més fàcil de trobar (error 402073)
- [kfilewidget] Retorna editable el navegador d'URL al mode fil d'Ariadna si té el focus i tot s'ha seleccionat, quan es prem Ctrl+L
- [KFileItem] Esmena la comprovació «isLocal» a «checkDesktopFile» (error 401947)
- SlaveInterface: Atura «speed_timer» després de matar el treball
- Avisa l'usuari abans d'un treball de copia/moure si la mida del fitxer excedeix la mida màxima possible de fitxer als sistemes de fitxers FAT32 (4 GB) (error 198772)
- Evita l'augment constant de la cua d'esdeveniments Qt als esclaus KIO
- Implementació del TLS 1.3 (part de les Qt 5.12)
- [KUrlNavigator] Esmena «firstChildUrl» en retornar des d'un arxiu

### Kirigami

- Assegura que no se substitueix QIcon::themeName quan no hauria
- Presenta un objecte annexat DelegateRecycler
- Esmena els marges de la vista de quadrícula considerant les barres de desplaçament
- Fa que AbstractCard.background reaccioni a AbstractCard.highlighted
- Simplifica el codi a MnemonicAttached
- SwipeListItem: mostra sempre les icones si «!supportsMouseEvents»
- Considera si «needToUpdateWidth» d'acord amb «widthFromItem», no l'alçada
- Té en compte la barra de desplaçament per al marge de ScrollablePage (error 401972)
- No intenta reposicionar la vista desplaçable quan s'obté una alçada defectuosa (error 401960)

### KNewStuff

- Canvia el sentit d'ordenació predeterminat al diàleg de baixada a «Més baixats» (error 399163)
- Notifica quan el proveïdor no s'ha carregat

### KNotification

- [Android] Falla de manera més elegant en construir amb una API &lt; 23
- Afegeix el dorsal de notificacions de l'Android
- Construeix sense el Phonon ni el D-Bus a l'Android

### KService

- applications.menu: elimina la categoria X-KDE-Edu-Teaching sense ús
- applications.menu: elimina &lt;KDELegacyDirs/&gt;

### KTextEditor

- Esmena la creació de scripts per a les Qt 5.12
- Esmena l'script Emmet perquè usi nombres HEX en lloc d'OCT a les cadenes (error 386151)
- Esmena l'Emmet trencat (error 386151)
- ViewConfig: Afegeix l'opció «Ajust dinàmic a marcador estàtic»
- Soluciona el final de la regió de plegat, afegeix un testimoni de final a l'interval
- Evita el sobrepintat lleig amb alfa
- No torna a marcar les paraules afegides/ignorades al diccionari com a errors ortogràfics (error 387729)
- KTextEditor: Afegeix una acció per l'ajust de paraula estàtic (error 141946)
- No oculta l'acció «Neteja l'abast dels diccionaris»
- No demana confirmació en recarregar (error 401376)
- Classe Message: Usa la inicialització del membre «inclass»
- Exposa KTextEditor::ViewPrivate:setInputMode(InputMode) a KTextEditor::View
- Millora el rendiment de les accions d'edició petites, p. ex. solucionant les accions grans de substituir-ho tot (error 333517)
- Només crida «updateView()» a «visibleRange()» quan la «endPos()» no és vàlida

### KWayland

- Afegeix una clarificació quant a l'ús de ServerDecoration i XdgDecoration del KDE
- Implementa la decoració XDG
- Soluciona la instal·lació de la capçalera Client del XDGForeign
- [server] Implementació tàctil d'arrossegar
- [server] Permet interfícies tàctils múltiples per a cada client

### KWidgetsAddons

- [KMessageBox] Esmena la mida mínima del diàleg quan se sol·liciten detalls (error 401466)

### NetworkManagerQt

- S'han afegit paràmetres de DCB, «macsrc», «match», «tc», «ovs-patch» i «ovs-port»

### Frameworks del Plasma

- [Calendar] Exposa «firstDayOfWeek» a MonthView (error 390330)
- Afegeix «preferences-system-bluetooth-battery» a «preferences.svgz»

### Purpose

- Afegeix el tipus de connector per compartir URL

### QQC2StyleBridge

- Esmena l'amplada dels elements de menú quan el delegat se substitueix (error 401792)
- Gira l'indicador d'ocupat en sentit horari
- Força que les caselles de selecció/radios siguin quadrats

### Solid

- [UDisks2] Usa MediaRemovable per determinar si el suport es pot expulsar
- Implementa les bateries del Bluetooth

### Sonnet

- Afegeix un mètode a BackgroundChecker per afegir una paraula a la sessió
- DictionaryComboBox: Manté a dalt els diccionaris preferits per l'usuari (error 302689)

### Ressaltat de la sintaxi

- Actualitza la implementació de la sintaxi del PHP
- WML: esmena el codi Lua incrustat i usa els estils predeterminats nous
- Ressaltat dels fitxers CUDA .cu i .cuh com a C++
- TypeScript &amp; TS/JS React: millora la detecció dels tipus, soluciona els punts flotants i altres millores/esmenes
- Haskell: Ressaltat dels comentaris buits després d'«import»
- WML: soluciona un bucle infinit en canvis de context i només ressalta les etiquetes amb noms vàlids (error 402720)
- BrightScript: Afegeix una solució temporal per al ressaltat «endsub» del QtCreator, afegeix el plegat de funció/sub
- Admet més variants de literals de nombres en C (error 402002)

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
