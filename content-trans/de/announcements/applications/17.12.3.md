---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE veröffentlicht die KDE-Anwendungen 17.12.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.12.3
version: 17.12.3
---
8. März 2018. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../17.12.0'>KDE-Anwendungen 17.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 25 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Dolphin, Gwenview, JuK, KGet, Okular und Umbrello.

Verbesserungen umfassen:

- Akregator no longer erases the feeds.opml feed list after an error
- Gwenview's fullscreen mode now operates on the correct filename after renaming
- Mehrere seltene Abstürze in Okular wurden gefunden und die Fehler behoben
