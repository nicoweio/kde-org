---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE lanza las Aplicaciones de KDE 15.08.2
layout: application
title: KDE lanza las Aplicaciones de KDE 15.08.2
version: 15.08.2
---
Hoy, 13 de octubre de 2015, KDE ha lanzado la segunda actualización de estabilización para las <a href='../15.08.0'>Aplicaciones 15.08</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 30 correcciones de errores registradas, se incluyen mejoras en Ark, Gwenview, Kate, Kdepim, Lokalize y Umbrello.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.13 que contará con asistencia a largo plazo.
