---
aliases:
- ../../kde-frameworks-5.22.0
date: 2016-05-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Comprobar adecuadamente si una URL es un archivo local

### Baloo

- Correcciones de errores de compilación para Windows

### Iconos Brisa

- Muchos iconos nuevos de acciones y aplicaciones
- Especificar las ampliaciones ofrecidas según el cambio en Kiconthemes

### Módulos CMake adicionales

- Desarrollo Android: admitir proyectos que no contengan nada en «share» ni en «lib/qml» (error 362578)
- Activar KDE_INSTALL_USE_QT_SYS_PATHS si se encuentra el prefijo CMAKE_INSTALL_PREFIX de Qt5
- ecm_qt_declare_logging_category: mejorar el mensaje de error cuando se usa sin incluir

### Integración con Frameworks

- Eliminar el complemento platformtheme como en plasma-integration

### KCoreAddons

- Proporcionar una manera de desactivar el uso de Inotify en KDirwatch
- Corregir KAboutData::applicationData() para que se inicie desde los medatos Q*Application actuales
- Aclarar que KRandom no se recomienda para fines de cifrado

### KDBusAddons

- KDBusService: convertir '-' en '_' en rutas de objetos

### KDeclarative

- Que no se produzca un fallo del sistema si no se dispone de contexto open GL

### Soporte de KDELibs 4

- Proporcionar una alternativa para MAXPATHLEN si no está definida
- Corregir KDateTime::isValid() para valores ClockTime (error 336738)

### KDocTools

- Se han añadido aplicaciones de entidad

### KFileMetaData

- Fusionar la rama «externalextractors»
- Se han corregido pruebas y complementos externos
- Se ha añadido compatibilidad para complementos externos de Writer
- Se ha añadido la compatibilidad con el complemento de Writer
- Se ha añadido la compatibilidad con el complemento de extracción externa

### KHTML

- Se ha implementado toString para Uint8ArrayConstructor y similares
- Fusionar varias correcciones relacionadas con Coverity
- Usar correctamente QCache::insert
- Corregir algunas fugas de memoria
- Comprobación de validez del análisis de tipos de letra del CSS para evitar posibles fugas de memoria
- dom: añadir prioridades de etiqueta para la etiqueta «comentar»

### KI18n

- libgettext: corregir el posible uso después de liberar en compiladores no g++

### KIconThemes

- Usar el contenedor adecuado para la matriz interna de punteros
- Añadir la oportunidad de reducir los accesos a disco innecesarios; introducir las extensiones KDE.
- Evitar algunos accesos a disco

### KIO

- kurlnavigatortoolbutton.cpp: usar buttonWidth en paintEvent()
- Nuevo menú de archivo: eliminar duplicados (por ejemplo, entre los archivos del sistema .qrc y ) (error 355390)
- Corregir el mensaje de error al iniciar el KCM de las cookies
- Eliminar kmailservice5, en este punto solo puede causar problemas (error 354151)
- Corregir KFileItem::refresh() para los enlaces simbólicos. Se estaban especificando tamaños, tipo de archivo y permisos incorrectos
- Corregir una regresión en KFileItem: refresh() perdía el tipo de archivo así que los directorios se volvían archivos (error 353195)
- Definir el texto en el elemento gráfico QCheckbox en lugar de utilizar una etiqueta aparte (error 245580)
- No activar el elemento gráfico de permisos acl si el usuario no es propietario del archivo (error 245580)
- Solucionar la doble barra en los resultados de KUriFilter cuando se define un filtro de nombre
- KUrlRequester: añadir la señal textEdited (renviada desde QLineEdit)

### KItemModels

- Corregir la sintaxis de la plantilla para la generación de casos de prueba
- Corregir el enlace con Qt 5.4 (#endif colocado incorrectamente)

### KParts

- Corregir la disposición del diálogo BrowserOpenOrSaveQuestion

### KPeople

- Añadir una comprobación de validez para PersonData

### KRunner

- Solucionar metainfo.yaml: KRunner no es una ayuda para la migración ni está obsoleto

### KService

- Eliminar la longitud máxima de cadena demasiado estricta en la base de datos de KSycoca

### KTextEditor

- Usar la sintaxis de carácter adecuada «'"'» en lugar de «'"'».
- doxygen.xml: usar el estilo predeterminado dsAnnotation también para «Etiquetas personalizadas» (menos colores fijos en el código)
- Añadir la opción de mostrar el contador de palabras
- Contraste de color mejorado para el resaltado de «Buscar y sustituir»
- Corregir el fallo del sistema cuando se cierra Kate a través de dbus mientras el diálogo de impresión está abierto (error #356813) (error 356813)
- Cursor::isValid(): añadir nota sobre isValidTextPosition()
- Añadir API {Cursor, Range}::{toString, static fromString}

### KUnitConversion

- Informar al cliente si se desconoce la relación de conversión
- Añadir la moneda ILS (nuevo shéquel israelí) (error 336016)

### Framework KWallet

- Desactivar la restauración de la sesión para kwalletd5

### KWidgetsAddons

- KNewPasswordWidget: eliminar el consejo sobre el tamaño en el espaciador, el cual dejaba siempre un espacio vacío en la disposición
- KNewPasswordWidget: corregir QPalette cuando el elemento gráfico está desactivado

### KWindowSystem

- Corregir la generación de la ruta al complemento xcb

### Framework de Plasma

- [QuickTheme] Corregir propiedades
- highlight/highlightedText a partir del grupo de color adecuado
- ConfigModel: no intentar resolver la ruta al origen vacía a partir del paquete
- [calendar] Mostrar la marca de evento solo en la vista de día, no en la de mes ni en la de año
- declarativeimports/core/windowthumbnail.h: corregir la advertencia de -Wreorder
- Recargar el tema de iconos correctamente
- Escribir siempre el nombre del tema en plasmarc, también si se elige el tema predeterminado
- [calendar] Añadir una marca a los días que contienen algún evento
- Añadir colores de texto Positivo, Neutro y Negativo
- ScrollArea: corregir la advertencia cuando contentItem no se puede hacer parpadeante
- Añadir un propietario y un método para alinear el menú con una esquina de su padre visual
- Permitir definir una anchura mínima en el Menú
- Mantener el orden en la lista de elementos almacenada
- Ampliar la API para permitir que las opciones de menú se recoloquen durante la inserción procedural
- Vincular el color highlightedText en Plasma::Theme
- Corregir la restauración de aplicaciones y URL asociadas para Plasma::Applets
- No exponer los símbolos de la clase privada DataEngineManager
- Añadir un elemento «Evento» en el SVG del calendario 
- SortFilterModel: invalidar el filtro cuando se cambia la llamada de retorno del filtro

### Sonnet

- Instalar la herramienta parsetrigrams para la compilación cruzada
- hunspell: cargar o guardar un diccionario personal
- Compatibilidad con hunspell 1.4
- configwidget: notificar el cambio de configuración cuando se actualiza la lista de palabras ignoradas
- preferencias: no guardar inmediatamente la configuración cuando se actualiza la lista de palabras ignoradas
- configwidget: corregir la función de guardar cuando se actualiza la lista de palabras ignoradas
- Corregir un fallo cuando se guardaba una palabra ignorada (error 355973)

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
