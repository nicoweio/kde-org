---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE toob välja KDE rakendused 18.08.3
layout: application
title: KDE toob välja KDE rakendused 18.08.3
version: 18.08.3
---
November 8, 2018. Today KDE released the third stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Umbes 20 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Arki, Dolphini, KDE mängude, Kate, Okulari ja Umbrello täiustused.

Täiustused sisaldavad muu hulgas:

- KMailis jäetakse nüüd HTML-i vaatamise režiim meelde ning lubamise korral laaditakse taas välised pildid
- Kate jätab nüüd seanssside vahel meelde metainfo (kaasa arvatud järjehoidjad)
- Automaatne kerimine Telepathy teksti kasutajaliideses parandati ära QtWebEngine'i uuema versiooniga
