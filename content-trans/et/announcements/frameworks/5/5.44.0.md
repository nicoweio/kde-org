---
aliases:
- ../../kde-frameworks-5.44.0
date: 2018-03-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl: valiku checkDb eemaldamine (veateade 380465)
- indexerconfig: mõningate funktsioonide kirjeldamine
- indexerconfig: funktsiooni canBeSearched avalikustamine (veateade 388656)
- balooctl monitor: ootamine dbus'i liidese järel
- fileindexerconfig: canBeSearched() pakkumine (veateade 388656)

### Breeze'i ikoonid

- view-media-playlist'i eemaldamine eelistuste ikoonide seast
- 24px media-album-cover'i ikooni lisamine
- Babe'i QML-i toetuse lisamine (22px)
- kirigami pidemeikoonide uuendamine
- elisa 64px meediaikoonide lisamine

### CMake'i lisamoodulid

- Define **ANDROID_API**
- readelf'i käsu nime parandus x86 peal
- Androidi tööriistaahel: muutuja ANDROID_COMPILER_PREFIX lisamine, x86 sihtmärkide kaasatute asukoha parandus, NDK sõltuvuste otsingutee laiendamine

### KDE Doxygeni tööriistad

- Lõpetamine tõrkega, kui väljundkataloog ei ole tühi (veateade 390904)

### KConfig

- Mõne mälueralduse säästmine õige API kasutamisega
- kconf_update'i eksport koos rakistusega

### KConfigWidgets

- KLanguageButton::insertLanguage täiustus, kui nime pole edastatud
- KStandardAction'ite valiku tühistamine (Deselect) ja asendamine (Replace) ikoonide lisamine

### KCoreAddons

- m_inotify_wd_to_entry puhastamine enne sisestusviitade märkimist kehtetuks (veateade 390214)
- kcoreaddons_add_plugin: tulutute OBJECT_DEPENDS'ide eemaldamine json-failist
- automoc'il aidatakse leida koodis viidatud metaandmete JSON-failid
- kcoreaddons_desktop_to_json: genereeritud faili äramärkimine ehituslogis
- shared-mime-info uuendati versioonile 1.3
- K_PLUGIN_CLASS_WITH_JSON'i pakkumine

### KDeclarative

- Ehitamise nurjumise parandus armhf/aarch64 peal
- QmlObjectIncubationController'i tapmine
- render() lahutamine akna muutumisel (veateade 343576)

### KHolidays

- Allen Winter on nüüd ametlik KHolidays'i hooldaja

### KI18n

- API dox: märkuse lisamine setApplicationDomain'i väljakutsumise kohta pärast QApp'i loomist

### KIconThemes

- [KIconLoader] ülekatete puhul võetakse arvesse devicePixelRatio

### KIO

- Ei eeldata msghdr ja iovec struktuuri paigutust (veateade 391367)
- Protokolli valiku parandus KUrlNavigator'is
- qSort'i muutmine std::sort'iks
- [KUrlNavigatorPlacesSelector] KFilePlacesModel::convertedUrl'i kasutamine
- [Kukutamistöö] kohase prügikastifaili loomine linkimisel
- tahtmatu liikumismenüü aktiveerimise parandus (veateade 380287)
- [KFileWidget] asukohtade raami ja päise peitmine
- [KUrlNavigatorPlacesSelector] kategooriate asetamine alammenüüdesse (veateade 389635)
- Standardse KIO testiabilise päise kasutamine
- Ctrl+H lisamine "peidetud failide näitamine/peitmine" kiirklahvide loendisse (veateade 390527)
- Liigutamise semantika toetuse lisamine KIO::UDSEntry'sse
- "Mitmetähendusliku kiirklahvi" probleemi parandus, mille tekitas D10314
- Teatekasti"Täitmisfaili ei leitud" täitmine järjekorras lambdadega (veateade 385942)
- Dialoogi "Avamine rakendusega" kasutatavuse täiustus võimalusega filtreerida rakendustepuud
- [KNewFileMenu] KDirNotify::emitFilesAdded pärast storedPut'i (veateade 388887)
- Eelduse parandus rebuild-ksycoca dialoogi töö katkestamisel (veateade 389595)
- Veateate 382437 parandus:"tagasilangus kdialog'is põhjustab vale faililaiendi"
- simplejob'i kiirem käivitus
- Faili VFAT-ile ilma hoiatusteta kopeerimine parandus
- kio_file: tõrkekäitluse vahelejätmine algsete õiguste määramisel faili kopeerimise korral
- KFileItem'ile lubatakse genereerida liikumise semantikat. Kompilaator genereerib samuti olemasoleva kopeerimise konstruktori, destruktori ja kopeerimise omistamise operaaatori.
- stat(/etc/localtime) jäetakse failide kopeerimisel read() ja write() vahel tegemata (veateade 384561)
- remote: tühjade nimedega kirjeid ei looda
- Võimaluse supportedSchemes lisamine
- F11 kasutamine kriiklahvina eelvaatluse kõrval näitamine lülitamiseks
- [KFilePlacesModel] jagatud võrguressursside rühmitamine kategooria "Kaugasukohad" alla

### Kirigami

- Tööriistanupu näitamine märgituna, kui näidatakse menüüd
- mitteinteraktiivsed kerimisriba märgised mobiilis
- Toimingute alammenüüde parandus
- Võimaldamine kasutada QQC2.Action'it
- Eksklusiivsete toimingugruppide toetuse võimaldamine (veateade 391144)
- Leheküljetoimingute tööriistanuppude teksti näitamine
- Toimingutel võimaldatakse näidata alammenüüd
- Eellases ei ole tarvis konkreetse komponendi asukohta
- SwipeListItem'i toiminguid ei käivitata, kui neid ei ole avalikustatud
- Kontrolli isNull() lisamine enne määramist, kas QIcon on mask
- FormLayout.qml'i lisamine kirigami.qrc'sse
- swipelistitem'i värvide parandus
- päiste ja jaluste parem käitumine
- ToolBarApplicationHeader'i vasakpoolse polsterduse ja kärpimise käitumise täiustus
- Tagamine, et liikumisnupud ei satuks toimingu alla
- päise ja jaluse omaduste toetus overlaysheet'is
- Tarbetu alapolsterduse likvideerimineOIverlaySheets'is (veateade 390032)
- ToolBarApplicationHeader'i välimuse viimistlemine
- sulgemisnupu näitamine töölaual (veateade 387815)
- arvutustabelit ei ole võimalik sulgeda hiirerattaga
- ikoonisuurust mitmekordistatakse ainult siis, kui Qt seda juba ei tee (veateade 390076)
- pideme positsiooni juures võetakse arvesse globaalset jalust
- sündmus tihendamaks kerimisribade loomist ja hävitamist
- Kerimisvaade: kerimisriba reegli muutmine avalikuks ja selle parandus

### KNewStuff

- vokoscreen'i lisamine KMoreTools'i ja selle lisamine rühma "screenrecorder"

### KNotification

- QWidget'i kasutamine nägemaks, kas aken on nähtav

### KPackage raamistik

- automoc'il aidatakse leida koodis viidatud metaandmete JSON-failid

### KParts

- Vana, kättesaamatu koodi puhastamine

### KRunner

- krunner'i pluginamalli uuendamine

### KTextEditor

- KTextEditor'i dokumendi ekspordi, järjehoidja eemaldamise ning teksti suurtähelise, väiketähelise ja pealkirjastiilis vormindamise ikoonide lisamine

### KWayland

- client-freed väljundi väljalaskmise toetus
- [server] kohane olukorra käitlemine, kui lohistamiseks mõeldud andmeallikas hävitatakse (veateade 389221)
- [server] krahhi vältimine, kui pakutakse välja pind, mille eellaspind on hävitatud (veateade 389231)

### KXMLGUI

- QLocale'i sisemine lähtestamine, kui meil on kohandatud rakenduse keel
- Eraldaja toiminguid ei lubata seadistada kontekstimenüü kaudu
- Paremklõpsu puhul väljaspool ei näidata kontekstimenüüd (veateade 373653)
- KSwitchLanguageDialogPrivate::fillApplicationLanguages'i täiustus

### Oxygeni ikoonid

- Artikulate ikooni lisamine (veateade 317527)
- folder-games'i ikooni lisamine (veateade 318993)
- calc.template'i vale 48px ikooni parandus (veateade 299504)
- media-playlist-repeat'i ja shuffle'i ikooni lisamine (veateade 339666)
- Oxygen: sildiikoonide lisamine nagu breeze'is (veateade 332210)
- emblem-mount'i linkimine media-mount'iga (veateade 373654)
- võrguikoonide lisamine, mis on saadaval breeze'i ikoonidesse (veateade 374673)
- oxygen'i ja breeze'i ikoonide sünkroonimine ja heliplasmoidi ikoonide lisamine
- edit-select-none'i lisamine Oxygen'iler Krusader'i jaoks (veateade 388691)
- rating-unrated'i ikooni lisamine (veateade 339863)

### Plasma raamistik

- largeSpacing'i uue väärtuse kasutamine Kirigamis
- PC3 TextField'i kohatäiteteksti nähtavuse vähendamine
- Ka tiitleid ei muudeta 20% läbipaistvaks
- [PackageUrlInterceptor] inline'i" ei kirjutata ümber
- Pealkirju ei muudeta 20% läbipaistavaks sobitumaks Kirigamiga
- hüpikusse ei asetata fullrep'i, kui see pole kokku keritud
- automoc'il aidatakse leida koodis viidatud metaandmete JSON-failid
- [AppletQuickItem] apletilaiendaja eellaadimine ainult siis, kui pole juba laiendatud
- muud eellaadimise pisioptimeerimised
- IconItem'ile määratakse vaikeväärtus smooth=true
- ka laiendaja (dialoogi) eellaadimine
- [AppletQuickItem] vaikimisi eellaadimise reegli parandus, kui keskkonnamuutujat ei ole määratud
- RTL-i välimuse parandus liitkastis (veateade https://bugreports.qt.io/browse/QTBUG-66446)
- teatud aplette püütakse eellaadida nutikal viisil
- [Icon Item] Filtreerimise määramine FadingNode'i tekstuurile
- m_actualGroup'i initsialiseerimine NormalColorGroup'ile
- Tagamine, et FrameSvg ja Svg isenditel oleks õige devicePixelRatio

### Prison

- Sõltuvuste linkide uuendamine ja Androidi märkimine ametlikult toetatuks
- DMTX sõltuvus ei ole enam kohustuslik
- QML-i toetuse lisamine Prison'ile
- Miinimumsuuruse määramine ka 1D ribakoodidele

### Purpose

- Kihi parandus, kohandamine KIO-ga

### QQC2StyleBridge

- Süntaksitõrke parandus eelmises sissekandes, mis avastati ruqola käivitamisel
- Raadionupu näitamine, kui meile kuulub eksklusiivne kontroll (veateade 391144)
- MenuBarItem'i teostus
- DelayButton'i teostus
- Uus komponent: ümar nupp
- tööriistariba asukohaga arvestamine
- nuppude ikoonide värvide toetus
- -reverse toetus
- Menüü ikoonid täisfunktsionaalsusega
- ühtased varjud uues breeze'i stiilis
- Mõni QStyle ei paista tagastavat mõistlikku pikselmeetrikat
- esimene rohmakate ikoonide toetus
- hiirerattaga üle otsa ei minda

### Solid

- lekke ja vale nullptr kontrolli parandus DADictionary's
- [UDisks] automaatse ühendamise tagasilanguse parandus (veateade 389479)
- [UDisksDeviceBackend] mitmekordse otsimise vältimine
- Mac/IOKit'i taustaprogramm: ketaste ja kettakogumite toetus

### Sonnet

- Locale::name() kasutamine Locale::bcp47Name() asemel
- msvc libhunspell'i ehituse leidmine

### Süntaksi esiletõstmine

- PHP ja Pythoni tarastatud koodiplokkide põhitoetus Markdown'is
- Tõstutundmatu WordDetect'i toetus
- Scheme esiletõstmine: koodi kirjutatud värvide eemaldamine
- SELinux CIL reeglite ja failikonteksti süntaksi esiletõstmise lisamine
- ctp faililaiendi lisamine PHP süntaksi esiletõstmisse
- Yacc/Bison: sümboli $ parandus ja Bisoni süntaksi uuendus
- awk.xml: gawk'i laienduse võtmesõnade lisamine (veateade 389590)
- APKBUILD'i esiletõstmine järgib Bash'i faili
- "APKBUILD'i esiletõstmine järgib Bash'i faili" tagasivõtmine
- APKBUILD'i esiletõstmine järgib Bash'i faili

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
