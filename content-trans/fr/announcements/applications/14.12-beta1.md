---
aliases:
- ../announce-applications-14.12-beta1
custom_spread_install: true
date: '2014-11-06'
description: KDE publie la version 14.12 « bêta » 1 des applications.
layout: application
title: KDE publie la première version « Beta » pour les applications KDE en version
  14.12.
---
06 Novembre 2014. Aujourd'hui, KDE publie la version « bêta » des applications de KDE. Les dépendances et les fonctionnalités sont stabilisées et l'équipe KDE se concentre à présent sur la correction des bogues et sur les dernières finitions.

Avec toutes les applications reposant sur KDE Frameworks 5, les publications de KDE Applications 14.12 nécessitent des tests poussés pour maintenir et améliorer la qualité ainsi que l'expérience utilisateur. Les utilisateurs ont un grand rôle à jouer dans la qualité de KDE car les développeurs ne peuvent tout simplement pas tester toutes les configurations possibles. Nous comptons sur vous pour nous aider à détecter les bogues suffisamment tôt pour pouvoir les corriger avant la livraison finale. N'hésitez pas à rejoindre l'équipe en installant la beta <a href='https://bugs.kde.org/'>et en signalant les bogues</a>.

#### Installation des paquets binaires des applications KDE 14.12 Beta 1

<em>Paquets</em>Quelques fournisseurs de systèmes d'exploitation Linux / Unix fournissent gracieusement pour certaines versions de leurs distributions, des paquets binaires pour les applications 14.12 « bêta » 1 (en interne 14.11.80). Dans d'autres cas, des bénévoles de la communauté le font aussi. Des paquets binaires supplémentaires, et également des mises à jour des paquets actuels, seront éventuellement mis à disposition dans les prochaines semaines.

<em>Emplacements des paquets</em>. Pour obtenir une liste à jour des paquets binaires disponibles, connus par l'équipe de publication KDE, veuillez visiter le <a href='http://community.kde.org/KDE_SC/Binary_Packages'> Wiki de la communauté</a>.

#### Compilation des applications KDE 14.12 Beta 1

Le code source complet des applications 14.12 « Bêta 1 » de KDE peut être <a href='http://download.kde.org/unstable/applications/14.11.80/src/'>librement téléchargé</a>. Toutes les instructions de compilation et d'installation sont disponibles sur la <a href='/info/applications/applications-14.11.80'>page d'informations des applications « Bêta 1 »</a>.
