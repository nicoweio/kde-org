---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Corrixir a xestión de límite e desprazamento en SearchStore::exec
- Recrear o índice de Baloo
- balooctl config: engadir opcións para definir ou ver onlyBasicIndexing
- Migrar a comprobación de balooctl para funcionar coa nova arquitectura (fallo 353011)
- FileContentIndexer: corrixir a emisión dupla de filePath
- UnindexedFileIterator: mtime é quint32, non quint64
- Transaction: corrixir outro erro ortográfico de Dbi
- Transaction: Corrixir o uso do Dbis incorrecto en documentMTime() e documentCTime().
- Transaction::checkPostingDbInTermsDb: Optimizar o código
- Corrixir avisos de D-Bus
- Balooctl: Engadir a orde checkDb
- balooctl config: Engadir «exclude filter»
- KF5Baloo: Asegurarse de que as interfaces de D-Bus se xeran antes de usalas. (fallo 353308)
- Evitar usar QByteArray::fromRawData
- Retirar baloo-monitor de baloo
- TagListJob: Emitir un erro cando non se poda abrir a base de datos
- Non ignorar subtermos se non se atopan
- Código máis limpo cando Baloo::File::load() falla ao fallar a apertura da base de datos.
- Facer que balooctl use IndexerConfig en vez de manipular baloofilerc directamente
- Mellorar a internacionalización de balooshow
- Facer que balooshow xestione ben o fallo se a base de datos non se pode abrir.
- Que Baloo::File::load() falle se a base de datos non está aberta. (fallo 353049)
- IndexerConfig: engadir o método refresh()
- inotify: Do not simulate a closedWrite event after move without cookie
- ExtractorProcess: Remove the extra n at the end of the filePath
- baloo_file_extractor: chamar a QProcess::close antes de destruír QProcess
- baloomonitorplugin/balooctl: internacionalizar o estado do indexador.
- BalooCtl: Engadir unha opción «config»
- Facer baloosearch máis presentábel
- Retirar ficheiros baleiros de EventMonitor
- BalooShow: Mostrar máis información cando os identificadores non coinciden
- BalooShow: Cando se chama cun identificador comprobar que sexa correcto
- Engadir unha clase FileInfo
- Engadir comprobacións de erros en varias partes para que Baloo non quebre cando se desactiva. (fallo 352454)
- Corrixir que Baloo non respectase a opción de configuración «só indexación básica»
- Monitor: Obter o tempo restante no inicio
- Usar chamadas reais a métodos en MainAdaptor en vez de QMetaObject::invokeMethod
- Engadir a interface org.kde.baloo ao obxecto raíz para manter compatibilidade cara atrás
- Corrixir a cadea de data que se mostra na barra de enderezo por migrar a QDate
- Engadir un atraso tras cada ficheiro en vez de só tras o lote
- Retirar a dependencia de Qt::Widgets de baloo_file
- Retirar código non usado de baloo_file_extractor
- Engadir un monitor de Baloo ou un complemento experimental de QML
- Facer compatíbel con fíos a consulta do tempo restante
- kioslaves: Engadir unha sobreposición que faltaba para funcións virtuais
- Extractor: definir applicationData tras construír a aplicación
- Query: engadir compatibilidade con «offset»
- Balooctl: Engadir --version e --help (fallo 351645)
- Retirar a compatibilidade con KAuth para aumentar o número máximo de vixías de inotify se o número é demasiado baixo (fallo 351602)

### BluezQt

- Corrixir a quebra de fakebluez en obexmanagertest con ASAN
- Declarar de maneira anticipada todas as clases exportadas en types.h
- ObexTransfer: Estabelecer o erro cando a sesión de transferencia se retira
- Utils: Manter punteiros a instancias de xestores
- ObexTransfer: Definir un erro cando org.bluez.obex quebra

### Módulos adicionais de CMake

- Actualizar a caché de iconas de GTK ao instalar iconas.
- Retirar un apaño para atrasar a execución en Android
- ECMEnableSanitizers: O sanitizador non definido é compatíbel con gcc 4.9
- Desactivar a detección de X11, XCB, etc. en OS X
- Buscar os ficheiros no prefixo instalado en vez da ruta do prefixo
- Usar Qt 5 para indicar cal é o prefixo de instalación de Qt 5
- Engadir a definición ANDROID onde se necesite en qsystemdetection.h.

### Integración de infraestruturas

- Corrixir o problema polo que de maneira aleatoria un diálogo de ficheiro non aparecía. (fallo 350758)

### KActivities

- Usar unha función de coincidencia personalizada en vez do glob de sqlite. (fallo 352574)
- Corrixiuse un problema engadindo un novo recurso ao modelo

### KCodecs

- Corrixir unha quebra en UnicodeGroupProber::HandleData con cadeas curtas

### KConfig

- Marcar kconfig-compiler como ferramenta sen interface gráfica de usuario

### KCoreAddons

- KShell::splitArgs: só o espazo ASCII é un separador, non o espazo Unicode U+3000 (fallo 345140)
- KDirWatch: corrixir unha quebra cando un destrutor estático global usa KDirWatch::self() (fallo 353080)
- Corrixir unha quebra cando se usa KDirWatch en Q_GLOBAL_STATIC.
- KDirWatch: corrixir a compatibilidade con fíos
- Clarificar como definir os argumentos do construtor de KAboutData.

### KCrash

- KCrash: pasar «cwd» a «kdeinit» ao iniciar de novo automaticamente a aplicación mediante kdeinit (fallo 337760).
- Engadir KCrash::initialize() para que as aplicacións e o complemento de plataforma poidan activar KCrash de maneira explícita.
- Desactivar ASAN se se activa

### KDeclarative

- Pequenas melloras en ColumnProxyModel
- Permitir que as aplicacións saiban a ruta de homeDir
- mover EventForge do contedor de escritorio
- Fornecer a propiedade enabled para QIconItem.

### KDED

- kded: simplificar a lóxica arredor de sycoca; chamar simplemente a ensureCacheValid.

### Compatibilidade coa versión 4 de KDELibs

- Chamar a newInstance desde o fillo na primeira chamada
- Usar definicións de kdewin.
- Non intentar atopar X11 en WIN32
- cmake: Corrixir a comprobación de versión de taglib en FindTaglib.cmake.

### KDesignerPlugin

- Qt moc non pode xestionar macros (QT_VERSION_CHECK)

### KDESU

- kWarning → qWarning

### KFileMetaData

- fornecer metadatos de usuario para Windows

### Complementos de interface gráfica de usuario de KDE

- Non buscar X11 e XCB tamén ten sentido para WIN32

### KHTML

- Substituír std::auto_ptr por std::unique_ptr
- khtml-filter: Descartar regras que conteñan funcionalidades de bloqueo de publicidade especiais que aínda non permitimos.
- khtml-filter: Reorganización de código sen cambios funcionais.
- khtml-filter: Ignorar expresións regulares con opcións xa que non as permitimos.
- khtml-filter: Corrixir a detección do delimitador das opcións de bloqueo de publicidade.
- khtml-filter: Borrar espazos en branco sobrantes.
- khtml-filter: Non descartar liñas que empecen por «&amp;» xa que non é un carácter especial de bloqueo de anuncios.

### KI18n

- retirar os iteradores estritos para msvc para que ki18n se constrúa

### KIO

- KFileWidget: o argumento «parent» debería ter 0 como valor predeterminado como en todos os trebellos.
- Asegurarse de que o tamaño da matriz de bytes que acabamos de soltar na estrutura ten tamaño dabondo antes de calcular o targetInfo, senón estamos accedendo a memoria que non nos pertence
- Corrixir o uso de Qurl ao chamar a QFileDialog::getExistingDirectory()
- Actualizar a lista de dispositivos de Solid antes de consultala en kio_trash
- Permitir «trash:» ademais de «trash:/» como URL para listDir (chama a listRoot) (fallo 353181)
- KProtocolManager: corrixir un bloqueo indefinido ao usar EnvVarProxy. (fallo 350890)
- Non intentar atopar X11 en WIN32
- KBuildSycocaProgressDialog: usar o indicador de actividade incluído en Qt. (fallo 158672)
- KBuildSycocaProgressDialog: executar kbuildsycoca5 con QProcess.
- KPropertiesDialog: corrixir nos casos nos que ~/.local é unha ligazón simbólica, comparar as rutas canónicas
- Engadir compatibilidade con comparticións de rede en kio_trash (fallo 177023)
- Conectar cos sinais de QDialogButtonBox, non de QDialog (fallo 352770)
- KCM de Cookies: actualizar os nomes de D-Bus para kded5
- Usar ficheiros JSON directamente en vez de kcoreaddons_desktop_to_json()

### KNotification

- Non enviar o sinal de actualización de notificación dúas veces
- Analizar de novo a configuración de notificación só cando cambia
- Non intentar atopar X11 en WIN32

### KNotifyConfig

- Cambiar o método para cargar os valores predeterminados
- Enviar o nome de aplicativo cuxa configuración se actualizou co sinal de D-Bus
- Engadir un método para reverter os valores predeterminados de kconfigwidget
- Non sincronizar a configuración n veces ao gardar

### KService

- Usar a maior data do subdirectorio como data do directorio do recurso.
- KSycoca: almacenar o mtime de todos os directorios de orixe para detectar cambios. (fallo 353036)
- KServiceTypeProfile: retirar unha creación de fábrica innecesaria. (fallo 353360)
- Simplificar e acelerar KServiceTest::initTestCase.
- facer o nome de instalación do ficheiro applications.menu unha variábel de CMake que se garda en caché
- KSycoca: ensureCacheValid() debería crear a base de datos se non existe
- KSycoca: facer que a base de datos global funcione tras o código de comprobación de data recente
- KSycoca: cambiar o nome de ficheiro da base de datos para incluír a linguaxe e o SHA1 dos directorios a partir dos cales se constrúe.
- KSycoca: facer ensureCacheValid() parte da API pública.
- KSycoca: engadir un punteiro q para retirar máis uso de singleton
- KSycoca: retirar todos os métodos self() para fábricas, almacenalos en KSycoca.
- KBuildSycoca: retirar a escritura do ficheiro ksycoca5stamp.
- KBuildSycoca: usar qCWarning en vez de fprintf(stderr, …) ou qWarning
- KSycoca: construír de novo ksycoca no proceso en vez de executar kbuildsycoca5
- KSycoca: mover todo o código de kbuildsycoca salvo main() á biblioteca.
- Optimización de KSycoca: só vixiar o ficheiro se a aplicación se conecta a databaseChanged()
- Corrixir fugas de memoria na clase KBuildSycoca
- KSycoca: substituír a notificación de D-Bus por vixilancia de ficheiro usando KDirWatch.
- kbuildsycoca: marcar a opción --nosignal como obsoleta.
- KBuildSycoca: substituír o bloqueo baseado en D-Bus por un ficheiro de bloqueo.
- Non quebrar ao atoparse con información de complemento incorrecta.
- Cambiar de nome as cabeceiras a _p.h en preparación para o movemento á biblioteca kservice.
- Mover checkGlobalHeader() dentro de KBuildSycoca::recreate().
- Retirar código de --checkstamps e --nocheckfiles.

### KTextEditor

- validar máis expresións regulares
- corrixir expresións regulares en ficheiros de salientado (fallo 352662)
- sincronizar o realce de ocaml co estado de https://code.google.com/p/vincent-hugot-projects/ antes de que Google Code deixe de estar dispoñíbel, algunhas correccións de erros pequenas
- engadir separación de palabras (fallo 352258)
- validar a liña antes de chamar a cousas de pregar (fallo 339894)
- Corrixir os problemas de conta de palabras de Kate escoitando a DocumentPrivate en vez de a Document (fallo 353258)
- Actualizar o realce de sintaxe de Kconfig: engadir novos operadores de Linux 4.2
- sincronizar coa rama «KDE/4.14» de kate
- minimap: Corrixir que non se debuxe a asa da barra de ferramentas ao desactivar as marcas de desprazamento (fallo 352641).
- sintaxe: Engadir a opción git-user a kdesrc-buildrc

### Infraestrutura de KWallet

- Deixar de pechar automaticamente no último uso

### KWidgetsAddons

- Corrixir o aviso C4138 (MSVC): «*/» atopado fóra dun comentario

### KWindowSystem

- Realizar unha copia profunda de get_stringlist_reply de QByteArray
- Permitir interactuar con varios servidores de X nas clases de NETWM.
- [xcb] Considerar as modificacións de KKeyServer inicializadas en plataformas distintas de X11
- Cambiar KKeyserver (x11) a rexistros con categoría

### KXMLGUI

- Permitir importar ou exportar esquemas de atallos de maneira simétrica

### NetworkManagerQt

- Corrixir as introspeccións, LastSeen debería estar en AccessPoint e non en ActiveConnection

### Infraestrutura de Plasma

- Agochar o diálogo de consello ao entrar o cursor nunha ToolTipArea inactiva
- se o ficheiro de escritorio ten Icon=/foo.svgz usar ese ficheiro do paquete
- engadir un tipo de ficheiro «screenshot» (captura de pantalla) nos paquetes
- ter en conta devicepixelration nas barras de desprazamento independentes
- retirar o efecto de cubrir en pantallas táctiles e móbiles
- Usar as marxes de SVG de lineedit no cálculo de sizeHint
- Non esvaer a icona nos consellos de Plasma
- Corrixir un texto de botón omitido
- Os menús contextuais de miniaplicativos dentro dun panel xa non se sobrepoñen co miniaplicativo
- Simplificar a obtención dunha lista de aplicacións asociadas en AssociatedApplicationManager

### Sonnet

- Corrixir o identificador de complemento de hunspell para cargalo correctamente
- permitir a compilación estática en Windows, engadir a ruta de dicionarios de Hunspell de LibreOffice en Windows
- Non asumir que os dicionarios de Hunspell están codificados en UTF-8 (fallo 353133).
- corrixir Highlighter::setCurrentLanguage() para o caso cando no que o idioma anterior era incorrecto (fallo 349151)
- permitir /usr/share/hunspell como ruta de dicionarios
- Complemento baseado en NSSpellChecker

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
