---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE rilascia KDE Applications 17.12.1
layout: application
title: KDE rilascia KDE Applications 17.12.1
version: 17.12.1
---
11 gennaio 2018. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../17.12.0'>KDE Applications 17.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Circa venti errori corretti includono, tra gli altri, miglioramenti a Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta e Umbrello.

I miglioramenti includono:

- L'invio della posta in Kontact è stata risolta per alcuni server SMTP
- La linea temporale e la ricerca di tag di Gwenview è stata migliorata
- L'importazione JAVA è stata risolta nello strumento per grafici UML Umbrello
