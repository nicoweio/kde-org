---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE Ships Applications 14.12.2.
layout: application
title: KDE에서 KDE 프로그램 14.12.2 출시
version: 14.12.2
---
February 3, 2015. Today KDE released the second stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

애너그램 게임 Kanagram, Umbrello UML Modeller, 문서 뷰어 Okular 및 가상 지구본 Marble에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.16, KDE Development Platform 4.14.5 and the Kontact Suite 4.14.5.
