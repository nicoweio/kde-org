---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE wydało Aplikacje KDE 15.04.3
layout: application
title: KDE wydało Aplikacje KDE 15.04.3
version: 15.04.3
---
1 lipca 2015. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../15.04.0'>Aplikacji KDE 15.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 20 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdenlive, kdepim, kopete, ktp-contact-list, marble, okteta oraz umbrello.

To wydanie zawiera także wersje o długoterminowym wsparciu: Przestrzeni Roboczych 4.11.21, Platformę Programistyczną KDE 4.14.10 oraz Pakiet Kontact 4.14.10 .
