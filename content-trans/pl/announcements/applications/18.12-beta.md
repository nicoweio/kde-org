---
aliases:
- ../announce-applications-18.12-beta
date: 2018-11-16
description: KDE wydało Aplikacje 18.12 Beta.
layout: application
release: applications-18.11.80
title: KDE wydało betę Aplikacji KDE 18.12
---
16 listopad 2018. Dzisiaj KDE wydało betę nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym polerowaniu.

Check the <a href='https://community.kde.org/Applications/18.12_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release

Aplikacje KDE, wydanie 18.12 potrzebuje wypróbowania go w szerokim zakresie, aby utrzymać i ulepszyć jakość i odczucia użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.
