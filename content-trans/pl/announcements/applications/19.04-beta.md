---
aliases:
- ../announce-applications-19.04-beta
date: 2019-03-22
description: KDE wydało Aplikacje 19.04 Beta.
layout: application
release: applications-19.03.80
title: KDE wydało Betę Aplikacji KDE 19.04
version_number: 19.03.80
version_text: 19.04 Beta
---
22 marca 2019. Dzisiaj KDE wydało wersję beta nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym udoskonalaniu.

Check the <a href='https://community.kde.org/Applications/19.04_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

Aplikacje KDE, wydanie 19.04 potrzebuje wypróbowania go w szerokim zakresie, aby utrzymać i ulepszyć jakość i odczucia użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.
