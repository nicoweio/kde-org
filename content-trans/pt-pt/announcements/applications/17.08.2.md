---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: O KDE Lança as Aplicações do KDE 17.08.2
layout: application
title: O KDE Lança as Aplicações do KDE 17.08.2
version: 17.08.2
---
12 de Outubro de 2017. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../17.08.0'>Aplicações do KDE 17.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 25 correcções de erros registadas incluem as melhorias no Kontact, no Dolphin, no Gwenview, no Kdenlive, no Marble, no Okular, entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.37.

As melhorias incluem:

- Foi corrigida uma fuga de memória e um estoiro na configuração do 'plugin' de eventos do Plasma
- As mensagens lidas não são mais removidas imediatamente do filtro Não-Lidas no Akregator
- O Importador do Gwenview agora usa a data/hora do EXIF
