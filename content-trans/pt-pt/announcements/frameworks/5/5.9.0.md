---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Novo módulo: ModemManagerQt (interface do Qt para a API do ModemManager). Em alternativa, actualize para a versão Plasma-NM 5.3 Beta ao actualizar para o ModemManagerQt 5.9.0.

### KActivities

- Implementou-se o esquecimento de um recurso
- Correcções de compilação
- Adição de um 'plugin' de registo de eventos para as notificações do KRecentDocument

### KArchive

- Respeito da definição do KZip::extraField também ao guardar os itens no cabeçalho central
- Remoção de duas validações erradas que ocorriam quando o disco estava cheio - 343214

### KBookmarks

- Correcção da compilação no Qt 5.5

### KCMUtils

- Novo sistema de 'plugins' baseado em JSON. Os KCMs são pesquisados e kcms/. Por agora, é necessário ainda instalar um ficheiro 'desktop' em kservices5/, por questões de compatibilidade.
- Correcção e envolvência da versão dos KCM's apenas em QML, se possível

### KConfig

- Correcção de validação ao usar o KSharedConfig num destruidor de objectos globais.
- kconfig_compiler: adição do suporte para o CategoryLoggingName nos ficheiros *.kcfgc, para gerar chamadas do qCDebug(categoria).

### KI18n

- pré-carregar o catálogo global do Qt ao usar o i18n()

### KIconThemes

- O KIconDialog pode agora ser apresentado com os métodos normais show() e exec() do QDialog
- Correcção do KIconEngine::paint para lidar com 'devicePixelRatios' diferentes

### KIO

- Permitir ao KPropertiesDialog mostrar a informação do espaço livre em disco dos sistemas de ficheiros remotos (p.ex., SMB)
- Correcção do KUrlNavigator com imagens de PPP's elevados
- Fazer o KFileItemDelegate tratar dos 'devicePixelRatios' não-predefinidos nas animações

### KItemModels

- KRecursiveFilterProxyModel: remodelação para emitir os sinais correctos nas alturas correctas
- KDescendantsProxyModel: Lidar com as movimentações relatadas pelo modelo de origem.
- KDescendantsProxyModel: Correcção do comportamento quando é efectuada uma selecção enquanto se reinicia.
- KDescendantsProxyModel: Permitir a construção e utilização do KSelectionProxyModel a partir do QML.

### KJobWidgets

- Propagação do código de erro para a interface de D-Bus do JobView

### KNotifications

- Adição de uma versão do event() que não recebe nenhum ícone e que usa um predefinido
- Adição de uma versão do event() que recebe o 'eventId' (StandardEvent9 e um 'iconName' (QString)

### KPeople

- Permitir o alargamento dos meta-dados de acções, usando os tipos predefinidos
- Correcção da não-actualização correcta do modelo após remover um contacto da entidade Person

### KPty

- Exposição global se o KPty foi compilado sem a biblioteca 'utempter'

### KTextEditor

- Adição do ficheiro de realce do kdesrc-buildrc
- sintaxe: adição do suporte para literais inteiros binários no ficheiro de realce do PHP

### KWidgetsAddons

- Tornar a animação do KMessageWidget suave com uma proporção de pixels do dispositivo elevada

### KWindowSystem

- Adição de uma implementação de testes em Wayland do KWindowSystemPrivate
- O KWindowSystem::icon com o NETWinInfo não está associado à plataforma X11.

### KXmlGui

- Preservação do domínio de traduções ao reunir os ficheiros .rc
- Correcção do aviso de execução 'QWidget::setWindowModified: O título da janela não contém um item de substituição '[*]''

### KXmlRpcClient

- Instalação das traduções

### Plataforma do Plasma

- Correcção de dicas inválidas quando o dono temporário da dica desaparecia ou ficava vazio
- Correcção do TabBar não ficar disposto adequadamente a título inicial, o que se verificava, por exemplo, no Kickoff
- As transições PageStack agora usam os Animators para animações mais suaves
- As transições TabGroup agora usam os Animators para animações mais suaves
- Fez-se com que o Svg,FrameSvg funcionasse com o QT_DEVICE_PIXELRATIO

### Solid

- Actualização das propriedades da bateria ao retomar de uma suspensão

### Mudanças no sistema de compilação

- Os Extra CMake Modules (ECM ou módulos-extra do CMake) têm agora a mesma numeração de versões como as Plataformas do KDE, sendo que agora estão na versão 5.9, quando anteriormente era a 1.8.
- Muitas plataformas foram corrigidas para poderem ser usadas sem procurar pelas suas dependências privadas. I.e., as aplicações que procurem por uma dada plataforma só precisam das suas dependências públicas, não das privadas.
- Permissão da configuração do SHARE_INSTALL_DIR para lidar melhor com as disposições multi-arquitectura

### Frameworkintegration

- Correcção de um estoiro possível ao destruir um QSystemTrayIcon (despoletado p.ex., pelo Trojitá) - erro 343976
- Correcção das janelas de ficheiros modais nativas no QML - erro 334963

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
