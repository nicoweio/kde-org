---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE levererar KDE-program 17.04.2
layout: application
title: KDE levererar KDE-program 17.04.2
version: 17.04.2
---
8:e juni, 2017. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../17.04.0'>KDE-program 17.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 15 registrerade felrättningar omfattar förbättringar av bland annat kdepim, ark, dolphin, gwenview och kdenlive.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.33.
