---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE levererar KDE-program 17.08.2
layout: application
title: KDE levererar KDE-program 17.08.2
version: 17.08.2
---
12:e oktober, 2017. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../17.08.0'>KDE-program 17.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 25 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Dolphin, Gwenview, Kdenlive, Marble och Okular.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.37.

Förbättringar omfattar:

- En minnesläcka och krasch i inställningen av Plasmas händelseinsticksprogram har rättats
- Lästa meddelanden tas inte längre omedelbart bort från filtret olästa i Akregator
- Gwenview import använder nu datum och tid från EXIF
