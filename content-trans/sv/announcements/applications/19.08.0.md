---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: KDE levererar Program 19.08.
layout: application
release: applications-19.08.0
title: KDE levererar KDE-program 19.08.0
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

KDE-gemenskapen är glada att tillkännage utgivningen av KDE Program 19.08.

Den här utgåvan ingår i KDE:s åtagande att kontinuerligt tillhandahålla förbättrade versioner av programmen vi levererar till våra användare. Nya versioner om Program ger fler funktioner och bättre konstruerad programvara som ökar användbarheten och stabiliteten hos program som Dolphin, Konsole, Kate, Okular och alla andra favoritverktyg i KDE. Vårt mål är att säkerställa att du förblir produktiv, och att göra KDE-programvara enklare och mer njutbar att använda för dig.

Vi hoppas att du blir nöjd med alla nya förbättringar som finns i 19.08.

## Vad är nytt i KDE-program 19.08

Mer än 170 fel har lösts. Rättningarna implementerar om inaktiverade funktioner, normaliserar genvägar och löser krascher, vilket gör programmen vänligare och låter dig arbeta och spela smartare.

### Dolphin

Dolphin är KDE:s fil- och katalogutforskare som du nu kan starta från varsomhelst genom att använda den nya globala genvägen <keycap>Meta+E</keycap>. Det finns också en ny funktion som minimerar röra på skrivbordet. När Dolphin redan kör, och du öppnar kataloger med ett annat program, visas katalogerna under en ny flik i det befintliga fönstret istället för i ett nytt Dolphin-fönster. Observera att beteendet är nu normalt på, men det kan inaktiveras.

Informationspanelen (som normalt är placerat till höger i Dolphins huvudpanel) har förbättrats på flera sätt. Du kan exempelvis välja att spela mediafiler automatiskt när de markeras i huvudpanelen, och du kan nu välja och kopiera texten som visas i panelen. Om du vill ändra vilken information panelen visar, kan du göra det direkt i panelen, eftersom Dolphin inte öppnar ett separat fönster när du väljer att ställa in panelen.

Vi har också löst många av irritationsmomenten och småfelen, som säkerställer att din erfarenhet av att använda Dolphin övergripande är mycket smidigare.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`Ny bokmärkesfunktion i Dolphin` caption=`Ny bokmärkesfunktion i Dolphin` width="600px" >}}

### Gwenview

Gwenview är KDE:s bildvisare, och i den här utgåvan har utvecklarna övergripande förbättras dess funktion för miniatyrbildsvisning. Gwenview kan nu använda "Låg resursanvändning" som läser in miniatyrbilder med låg upplösning (om tillgängliga). Det nya läget är mycket snabbare och resurssnålare när miniatyrbilder läses in för JPEG-bilder och obehandlade filer. I fall där Gwenview inte kan skapa en miniatyrbild för en bild, visas nu en platsmarkörbild istället för att återanvända miniatyrbilden för den föregående bilden. Problemen Gwenview hade med att visa miniatyrbilder från Sony- och Canon-kameror har också lösts.

Förutom ändringar i miniatyrbildsdepartementet, har Gwenview också implementerat en ny meny, "Dela", som tillåter att bilder skickas till olika platser, och läser in och visar filer från andra datorer hämtade via KIO. Den nya versionen av Gwenview visar också mycket mer Exif-metadata för obehandlade bilder.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`Den nya menyn "Dela" i Gwenview` caption=`Den nya menyn "Dela" i Gwenview` width="600px" >}}

### Okular

Utvecklare har introducerat många förbättringar för kommentarer i Okular, KDE:s dokumentvisare. Förutom det förbättrade användargränssnittet för inställningsdialogrutor av kommentarer, kan linjekommentarer nu ha diverse visuella prydnader tillagda i sluten, vilket låter dem exempelvis omvandlas till pilar. Den andra saken du kan göra med kommentarer är att expandera och dra ihop dem alla på en gång.

Okulars stöd för ePub-dokument har också fått en knuff i den här versionen. Okular kraschar inte längre vid försök att läsa in felformade ePub-dokument filer, och dess prestanda med stora ePub-filer har förbättrats signifikant. Listan över ändringar i den här versionen omfattar förbättrade sidokanter och presentationslägets markeringsverktyg med hög upplösning.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Okulars inställning av kommentarverktyg med det nya alternativet för linjeslut` caption=`Okulars inställning av kommentarverktyg med det nya alternativet för linjeslut` width="600px" >}}

### Kate

Tack vare våra utvecklare, har tre irriterande fel krossats i den här versionen av KDE:s avancerade texteditor. Kate flyttar återigen sitt befintliga fönster överst när det blir tillfrågat om att öppna ett nytt dokument av ett annat program. Funktionen \"Snabböppna\" sorterar objekt enligt senast använda, och väljer det översta alternativet i förväg. Den tredje ändringen är i funktionen \"Senaste dokument\", som nu fungerar när den aktuella inställningen är att inte spara inställningarna av individuella fönster.

### Konsole

Den mest märkbara ändring i Terminal, KDE:s terminalemuleringsprogram, är lyftet av funktionen för sida vid sida. Du kan nu dela huvudrutan på vilket sätt du vill, både vertikalt och horisontellt. Delrutorna kan sedan delas precis som du vill. Versionen lägger också till möjligheten att dra och släppa rutor, så att du enkelt kan arrangera om layouten för att passa ditt arbetsflöde.

Förutom det, har inställningsfönstret fått en översyn för att göra det klarare och enklare att använda.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

Spectacle är KDE:s skärmbildsprogram och det får fler och fler intressanta funktioner med varje ny version. Den här versionen är inget undantag, eftersom Spectacle nu levereras med flera nya funktioner som styr dess fördröjningsfunktion. När en tidsfördröjd skärmbild tas, visar Spectacle tiden som återstår på namnlisten. Informationen är också synlig i aktivitetshanteraren.

Fortfarande gällande fördröjningsfunktionen, visar också Spectacles knapp i aktivitetshanteraren också en förloppsrad, så att du kan hålla reda på när bilden tas. Och slutligen, om du återställer Spectacle från minimering medan du väntar, ser du att knappen "Ta en ny skärmbild" har omvandlats till knappen \"Avbryt\". Knappen innehåller också en förloppsrad, vilket ger dig chansen att stoppa nedräkningen.

Spara skärmbilder har också en ny funktion. När du har sparat en skärmbild, visar Spectacle en meddelande i programmet som låter dig öppna skärmbilden eller katalogen som innehåller den.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

Kontact, KDE:s e-post-, kalender- kontakt-, och allmänna gruppprogramsvit, har lagt till stöd för  Unicode färgemoji och Markdown i brevfönstret. Den nya versionen av Kmail låter dig göra att dina brev se bra ut, och, tack vara integrering med grammatikkontroll, såsom LanguageTool och Grammalecte, hjälper dig kontrollera och korrigera din text.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Väljare av emoji` caption=`Väljare av emoji` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`Integrering av grammatikkontroll i Kmail` caption=`Integrering av grammatikkontroll i Kmail` width="600px" >}}

Vid planering av möten, tas inte inbjudningsbrev i Kmail längre bort efter du svarat på dem. Det är nu möjligt att flytta en händelse från en kalender till en annan i händelseeditorn i Korganizer.

Sist med inte minst, kan Adressboken nu skicka SMS-brev till kontakter via KDE-anslut, vilket resulterar i en bekvämare integrering av skrivbordet och mobila apparater.

### Kdenlive

Den nya versionen av Kdenlive, KDE:s videoredigeringsprogramvara, har en ny uppsättning kombinationer av tangentbord och mus som hjälper dig bli produktivare. Du kan exempelvis ändra hastigheten på ett klipp i tidslinjen genom att trycka på Ctrl och sedan dra på klippet, eller aktivera miniatyrbildsförhandsgranskningen av videoklipp genom att hålla nere Skift och flytta musen över en miniatyrbildsklipp i projektkorgen. Utvecklarna har också lagt ner mycket ansträngning i användbarhet genom att göra trepunktsredigering likformig med andra videoeditorer, vilket du säkerligen uppskattar om du byter till Kdenlive från en annan editor.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
