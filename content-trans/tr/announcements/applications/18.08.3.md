---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE, KDE Uygulamaları 18.08.3'ü Gönderdi
layout: application
title: KDE, KDE Uygulamaları 18.08.3'ü Gönderdi
version: 18.08.3
---
November 8, 2018. Today KDE released the third stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen yaklaşık 20 hata düzeltmesi, diğerleri arasında Kontak, Ark, Dolphin, KDE Games, Kate, Okular, Umbrello'daki iyileştirmeleri içerir.

İyileştirmeler şunları içerir:

- K Posta'daki HTML görüntüleme kipi hatırlanır ve izin verilirse dış görselleri yeniden yükler
- Kate artık düzenleme oturumları arasındaki meta bilgileri (yer imleri dahil) hatırlıyor
- Telepati metin kullanıcı arayüzünde otomatik kaydırma, daha yeni QtWebEngine sürümleriyle düzeltildi
