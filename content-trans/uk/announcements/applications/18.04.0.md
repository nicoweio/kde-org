---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE випущено збірку програм KDE 18.04.0
layout: application
title: KDE випущено збірку програм KDE 18.04.0
version: 18.04.0
---
19 квітня 2018 року. Сьогодні KDE випущено збірку програм KDE 18.04.0.

Ми послідовно працюємо над поліпшенням програмного забезпечення, яке є частиною наших збірок програм KDE. Сподіваємося, нові удосконалення та виправлення вад стануть вам у пригоді!

## Нове у програмах KDE 18.04

### Система

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

Перший основний випуск у 2018 році <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, потужної програми для керування файлами у KDE. У новій версії можна скористатися багатьма поліпшеннями у роботі панелей програми:

- Розділи панелі «Місця» тепер можна приховувати, якщо вони вам не потрібні. Передбачено новий розділ «Мережа» для зберігання записів віддалених адрес.
- Панель «Термінал» можна пришвартувати до будь-якого боку вікна. Якщо ви спробуєте відкрити цю панель у системі, де не встановлено Konsole, Dolphin покаже попередження щодо цього і допоможе вам встановити програму.
- Поліпшено підтримку високої роздільності екрана на панелі «Інформація».

Крім того, оновлено режим перегляду тек та меню:

- У теці «Смітник» тепер показується кнопка «Спорожнити смітник».
- Додано пункт меню «Показати призначення», який допомагає знаходити об'єкти, на які посилаються символічні посилання.
- Розширено можливості інтеграції із git — у контекстному меню нової версії для тек git передбачено два нових пункти для команд «git log» і «git merge».

Серед інших удосконалень:

- Впроваджено нове клавіатурне скорочення, яке надає вам змогу відкривати панель фільтрування простим натисканням клавіші з похилою рискою (/).
- У новій версії можна упорядковувати фотографії за датою створення.
- Пришвидшено роботу засобу перетягування зі скиданням у Dolphin для наборів з багатьох малих файлів. Користувач тепер може скасовувати результати виконання завдань із пакетного перейменовування.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

Щоб працювати у командному рядку було ще приємніше, <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, емулятор термінала KDE, тепер має ще красивіший вигляд:

- Отримати схеми кольорів можна за допомогою засобів KNewStuff.
- Смужка гортання краще вписується до активної схеми кольорів.
- Типово, панель вкладок буде показано, лише якщо це справді потрібно.

Учасники розробки Konsole не зупинилися на цьому і впровадили нові можливості:

- Було додано новий режим («лише читання») та можливість вмикати або вимикати копіювання тексту у форматі HTML.
- У Wayland для Konsole тепер передбачено підтримку меню перетягування зі скиаданням.
- Декілька удосконалень стосуються протоколу ZMODEM: нова версія Konsole може працювати із індикатором вивантаження zmodem B01. Тепер програма зможе показувати поступ передавання даних, кнопка «Скасувати» у діалоговому вікні тепер працює як слід, а підтримку передавання великих файлів поліпшено впровадженням їхнього читання до пам'яті фрагментами у 1 МБ.

Серед інших удосконалень:

- Виправлено гортання коліщатком миші за допомогою libinput. Реалізовано запобігання циклічному переходу у журналі команд оболонки під час гортання коліщатком миші.
- Реалізовано оновлення засобів пошуку після зміни варіанта пошуку за формальними виразами, а натискання комбінації «Ctrl+Backspace» у Konsole тепер працює так само, як у xterm.
- Було виправлено роботу параметра «--background-mode».

### Звук та відео

У <a href='https://juk.kde.org/'>JuK</a>, музичному програвачі KDE, тепер передбачено підтримку Wayland. Серед нових можливостей можливість приховувати смужку меню і отримувати візуальні сповіщення щодо поточної композиції. Якщо вимкнено згортання до системного лотка, JuK більше не завершує роботу у аварійному режимі у відповідь на спробу вийти з програми за допомогою піктограми «Закрити», а інтерфейс користувача залишатиметься видимим.Також виправлено вади, пов'язані із неочікуваним відновленням відтворення у JuK після пробудження системи зі стану сну у Плазмі 5 та обробкою стовпчика даних списку відтворення.

У випуску 18.04 учасники розробки <a href='https://kdenlive.org/'>Kdenlive</a>, нелінійного відеоредактора KDE, зосередили свої зусилля на такому:

- Усуванні помилки, яка призводила до того, що зміна розмірів кліпу руйнувала переходи і ключові кадри.
- Користувачі масштабування дисплея на моніторах із високою роздільною здатністю тепер зможуть насолодитися красивішими піктограмами.
- Виправлено можливе аварійне завершення роботи під час запуску на деяких конфігураціях.
- Для збирання і роботи програми тепер потрібна бібліотека MLT версії 6.6.0 або новіша. Поліпшено сумісність із бібліотекою.

#### Графіка

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

Протягом останніх місяців учасники розробки <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, програми для перегляду та упорядковування зображень KDE, працювали над низкою удосконалень. Основними з них є такі:

- Додано підтримку контролерів MPRIS, отже, ви тепер зможете керувати показами слайдів за допомогою KDE Connect, мультимедійних клавіш вашої клавіатури та плазмоїда «Програвач мультимедійних даних».
- Кнопки дій на мініатюрах тепер можна вимикати.
- Було дещо удосконалено інструмент обрізання. Програма тепер запам'ятовує його параметри при перемиканні на інше зображення. Форму області вибору тепер можна заблокувати натисканням клавіші Shift або Ctrl. Крім того, її можна зафіксувати на пропорціях поточного показаного зображення.
- Тепер ви зможете виходити з повноекранного режиму за допомогою натискання клавіші Esc, а палітра кольорів у цьому режимі відповідатиме вашій поточній темі кольорів. Якщо ви вийдете з Gwenview у повноекранному режимі, програма запам'ятає це і наступний сеанс розпочнеться у повноекранному режимі.

Важливою є увага до деталей, тому Gwenview було поліпшено у таких аспектах:

- Нова версія Gwenview показує більше зручних для читання шляхів у списку «Нещодавні теки», показує належні контекстні меню для пунктів у списку «Нещодавні файли» і належним чином забуває про все, якщо ви скористаєтеся можливістю «Вимкнути журнал».
- Натискання пункту теки на бічній панелі надає змогу перемикатися між режимами навігації і перегляду. Програма запам'ятовує останній використаний режим при перемиканні між теками. Таким чином, у новій версії навігація величезними збірками зображень є швидшою.
- Навігацію за допомогою клавіатури ще більше спрощено належним позначенням фокуса клавіатури у режимі навігації.
- Варіант перегляду «Заповнити за шириною» було замінено загальнішим варіантом «Заповнити».

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Навіть незначні удосконалення часто роблять користування програмою приємнішим:

- Поліпшено однорідність перегляду: тепер зображення SVG збільшуються як усі інші зображення, якщо позначено пункт «Перегляд зображення → Збільшувати менші зображення».
- Після редагування зображення або скасовування змін більше не порушується синхронізація між панеллю перегляду зображень та мініатюрою.
- Під час перейменовування зображень суфікс назви у новій версії типово не позначатиметься, а типовою текою у діалоговому вікні пересування, копіювання та створення посилання буде поточна тека.
- Усунено багато дрібних помилок у інтерфейсі, наприклад на панелі адреси, панелі інструментів режиму повноекранного перегляду та у анімаціях панелі підказки для мініатюр. Було також додано пропущені піктограми.
- Нарешті, кнопка-накладка у режимі повноекранного перегляду тепер надає змогу безпосередньо переглянути зображення, а не відкриває панель вмісту теки, а за допомогою додаткових параметрів розширено можливості керування відтворенням кольорів за допомогою ICC.

#### Офіс

У <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, універсальній програмі для перегляду документів KDE, обробку зображення PDF та видобування тексту тепер можна скасувати, якщо у вас встановлено версію poppler 0.63 або новішу. Це означає, що, якщо ви маєте справу зі складним файлом PDF, і ви змінили масштаб під час обробки його зображення, ви можете негайно скасувати обробку, не чекаючи на завершення побудови зображення.

Ви зможете скористатися поліпшеною підтримкою PDF для AFSimple_Calculate. А якщо у вас встановлено версію poppler 0.64 або новішу, у Okular можна буде скористатися підтримкою зміни стану «лише читання» за допомогою JavaScript у формах PDF.

Значно розширено можливості із керування повідомленнями із підтвердженнями бронювання квитків у <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, потужному клієнті електронної пошти KDE. Тепер можна керувати замовленнями залізничних квитків і користуватися заснованою на даних Вікіпедії базою даних аеропортів для показу рейсів із врахуванням даних часових поясів. Для спрощення обробки було створено новий засіб видобування даних із повідомлень, які не містять структурованих даних щодо замовлення квитків.

Серед інших удосконалень:

- Структуру повідомлення знову можна переглянути за допомогою нового «експертного» додатка.
- До редактора фільтрів додано додаток для вибору повідомлень з бази даних Akonadi.
- Поліпшено можливості із пошуку тексту на основі формальних виразів у редакторі.

#### Інструменти

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

Удосконалено інтерфейс користувача <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, багатофункціональної програми для створення знімків вікон KDE. Зокрема внесено такі зміни:

- Виконано ревізію нижнього рядка кнопок. Тепер там показано кнопку для відкриття вікна параметрів програми і нову кнопку «Інструменти», яка відкриває меню із пунктами для відкриття останньої використаної теки для знімків та програми для запису відео з екрана.
- Програма тепер запам'ятовує останній використаний режим зберігання зображень.
- Розмір вікна програми тепер адаптується до пропорцій знімка вікна, що уможливлює красивіший показ мініатюри знімка із заощадженням місця на екрані.
- Значно спрощено вікно параметрів роботи програми.

Крім того, виконання користувачами завдань спрощено за допомогою нових можливостей:

- Якщо було зроблено знімок певного вікна, його заголовок може бути автоматично до назви файла знімка вікна.
- Користувач може тепер визначити, чи слід завершувати роботу Spectacle у автоматичному режимі після виконання певної дії із збереження або копіювання.

Серед важливих виправлень вад:

- Перетягування і скидання зображення до вікон Chromium у новій версії працює як слід.
- Повторне використання клавіатурних скорочень для зберігання знімка вікна більше не призводить до показу попередження щодо неоднозначного клавіатурного скорочення.
- Для знімків прямокутних ділянок тепер можна точніше визначити розташування нижнього краю знімка.
- Поліпшено надійність створення знімків вікон торканням країв сенсорного екрана, якщо композитне відтворення вимкнено.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

Нова версія <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, програми для керування сертифікатами та універсального графічного інтерфейсу шифрування KDE, може створювати ключі на основі кривої 25519 EdDSA, якщо використовується новітня версія GnuPG. До нової версії було додано панель «Нотатник» для криптографічних дій з текстом. Тепер ви можете підписати, зашифрувати, розшифрувати та перевірити шифрування текстових даних безпосередньо у вікні програми. На панель «Параметри сертифіката» додано кнопку експортування, якою ви можете скористатися для експортування текстових даних. Крім того, ви можете імпортувати результати експортування на нову панель «Нотатник».

У <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, графічному інтерфейсі KDE для керування файлами архівів із підтримкою багатьох форматів, тепер можна зупиняти стискання або пакування архівів ZIP, якщо використовується модуль обробки libzip.

### Програми долучаються до розкладу випуску комплекту програм KDE

Програма для керування вебкамерою KDE <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> та програма для резервного копіювання <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> тепер випускаються разом із іншими програмами комплекту програм KDE. Також до збірки знову включено програму для миттєвого обміну повідомленнями <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a>, яку було портовано на KDE Frameworks 5.

### Програми отримали власний розклад випусків

Шістнадцятковий редактор <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> тепер має власний графік випусків. Про це попросив його поточний супровідник.

### Усування вад

Було виправлено понад 170 вад у програмах, зокрема у комплексі програм Kontact Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello!

### Повний список змін
