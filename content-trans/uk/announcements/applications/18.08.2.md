---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE випущено збірку програм KDE 18.08.2
layout: application
title: KDE випущено збірку програм KDE 18.08.2
version: 18.08.2
---
11 жовтня 2018 року. Сьогодні KDE випущено друге стабільне оновлення <a href='../18.08.0'>набору програм KDE 18.08</a>. До цього випуску включено лише виправлення вад та оновлення перекладів. Його встановлення є безпечним і корисним для усіх користувачів.

Понад десяток зареєстрованих виправлень вад поліпшення у роботі Kontact, Dolphin, Gwenview, KCalc, Umbrello та інших програм.

Серед удосконалень:

- Перетягування файла до вікна Dolphin більше не призводить до ненавмисного започаткування процедури перейменовування
- У KCalc знову уможливлено використання обох клавіш, крапки і коми, для введення десяткових дробів
- Виправлено помилку у відтворенні колоди карт у карткових іграх KDE
