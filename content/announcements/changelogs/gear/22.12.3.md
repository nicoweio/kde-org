---
aliases:
- ../../fulllog_releases-22.12.3
title: KDE Gear 22.12.3 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-contacts" title="akonadi-contacts" link="https://commits.kde.org/akonadi-contacts" >}}
+ Fix bug 465669:  Untrimmed URL added to a contact get dropped. [Commit.](http://commits.kde.org/akonadi-contacts/aa6b4e741b5f35ddb1cc32c089d64e546aaf2c63)Fixes bug [#465669](https://bugs.kde.org/465669)
{{< /details >}}
{{< details id="akregator" title="akregator" link="https://commits.kde.org/akregator" >}}
+ Fix bug 465960: RSS feed logos are incorrectly scaled by title height. [Commit.](http://commits.kde.org/akregator/5d776e59c72b6f69326884337ee712f7d370323f)Fixes bug [#465960](https://bugs.kde.org/465960)
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Fix free space check. [Commit.](http://commits.kde.org/ark/6cbcbb4a7972c0887ab8e8be4c7ebd7ff5f27e00)Fixes bug [#459418](https://bugs.kde.org/459418)
+ ExtractJob: Fix calculation of totalUncompressedSize. [Commit.](http://commits.kde.org/ark/42177f3ec440d098f4adb336864cc353ca9ae4f1)
+ LoadJob: Fix ExtractedFilesSize calculation considering sparse files. [Commit.](http://commits.kde.org/ark/017976212018349205d400dea0e44995675d703b)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix empty music collection after rescan if baloo isn't available. [Commit.](http://commits.kde.org/elisa/c89ff103d04e7245809a7c2a5ccc7988cb79d092)Fixes bug [#417267](https://bugs.kde.org/417267)
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Find qml dep on Qt.labs.platform. [Commit.](http://commits.kde.org/filelight/64985d39b09cc8c17593c8e1ee089eb8d59e14ef)Fixes bug [#466013](https://bugs.kde.org/466013)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Share all selected URLs. [Commit.](http://commits.kde.org/gwenview/f59613940f7c6cf36adee99e659c5358ea4acae9)Fixes bug [#465137](https://bugs.kde.org/465137)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Elide too long stop names in the journey section view. [Commit.](http://commits.kde.org/itinerary/1af53eb33a99a80dc3ab9eb765b366cc4afa9453)
+ Fix TimelineSelectionDelegateControllerTest. [Commit.](http://commits.kde.org/itinerary/d44537120a40f788f754b3ef9149e9a685fcf75d)
+ Also show timezone information for UTC times when not being in UTC. [Commit.](http://commits.kde.org/itinerary/7708a1ce6bee10f4bee62b4a82e7640496b19727)
+ Forward compatibility with Qt 6. [Commit.](http://commits.kde.org/itinerary/ecb1fa93cf11c834703defb4b4113dd6b6f774ee)
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Add comment for the extra condition. [Commit.](http://commits.kde.org/kalendar/22e2cfa790077a111b471cf052ec7e4c786f69d8)
+ Correct if..else if structure. [Commit.](http://commits.kde.org/kalendar/55f28ad3bc60b44d1fb1ca40478ae0d485900fae)
+ Changed comparator function to handle the special case. [Commit.](http://commits.kde.org/kalendar/3608fedbc184785d20fdd896b97aa33e14ed2b28)Fixes bug [#455210](https://bugs.kde.org/455210)
+ Fix basic week view not updating when dates changed. [Commit.](http://commits.kde.org/kalendar/33cd0e6f858dafdd35b683a0a885aedc3a7ee85b)
+ Clarify where periodStartDate comes from. [Commit.](http://commits.kde.org/kalendar/89b55f34a422de83ad9ee1685086727fd133b657)
+ Fix bug when droping event with the same time. [Commit.](http://commits.kde.org/kalendar/8d70d95ab61d403e39fd1b2d34da72b214d8eeb8)Fixes bug [#465080](https://bugs.kde.org/465080)
+ Fix loading contact editor. [Commit.](http://commits.kde.org/kalendar/1c6ad376332e75e20655f0b146f74208992761d1)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Activate the view of viewspace which made the request. [Commit.](http://commits.kde.org/kate/6a28dfe19247a7a02a5e08e9bfa245ec4c10280e)Fixes bug [#465811](https://bugs.kde.org/465811)
+ Save global options of active window before creating new. [Commit.](http://commits.kde.org/kate/a84fbe4910230ef029c40c19c85aa6a402622370)See bug [#464639](https://bugs.kde.org/464639). Fixes bug [#465064](https://bugs.kde.org/465064)
+ Sessions: Save session on document open and close. [Commit.](http://commits.kde.org/kate/a11139306939baf66d566de60a5a1e11df53db65)Fixes bug [#437337](https://bugs.kde.org/437337)
+ Ctags: Wrap with quotes. [Commit.](http://commits.kde.org/kate/6e9eccd4915415e74610a5d1eb4b1e31e04fc18d)Fixes bug [#378322](https://bugs.kde.org/378322)
+ Fix leaking actions in welcomeview. [Commit.](http://commits.kde.org/kate/2c922ea079efbaae9fdbab16c6af253a6369559e)
+ Fix enter press on a dir after filtering. [Commit.](http://commits.kde.org/kate/1f6954257ebec8cb450d0c07f7ebea1a45303edf)
+ Fix urlbar doesn't select first item on filtering. [Commit.](http://commits.kde.org/kate/479b41545c1729bd5796dc0bb12690cb24ba3749)Fixes bug [#464640](https://bugs.kde.org/464640)
+ Tooltips: Use QFrame::Box. [Commit.](http://commits.kde.org/kate/917975c5e20e8056a03a133755cdbd0e23dce8c3)
+ ProjectTerminal: Open new tab in same directory as existing tab. [Commit.](http://commits.kde.org/kate/ef34bed8ea1c0f41d5cb14e4ead0588fd89361a2)
+ Fix widget closing when it has no tab. [Commit.](http://commits.kde.org/kate/c4a575b0d84b36e9a02833edf27a1a359a04ab7a)
+ Fix widget activation. [Commit.](http://commits.kde.org/kate/67b0e6d945a486ecb3942e43f4effb688597177c)
+ Build: Prevent crash where the tab-widget is null. [Commit.](http://commits.kde.org/kate/0c73da2dd5fdbcb7a65193d82815c02c55a1e09a)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Nautilus: drop spurious shebang. [Commit.](http://commits.kde.org/kdeconnect-kde/71803f04d8521199dfa6960f5b84801d9f35d9ee)
{{< /details >}}
{{< details id="kdenetwork-filesharing" title="kdenetwork-filesharing" link="https://commits.kde.org/kdenetwork-filesharing" >}}
+ Samba: Fix build with GCC 13 (add missing <cstdint> include). [Commit.](http://commits.kde.org/kdenetwork-filesharing/3a0cf57550e0b3faa53ad431cbe086956662663d)
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix subtitle scrolling. [Commit.](http://commits.kde.org/kdenlive/bdbf0bf5122ae5fce7334203a7d990dd7032a129)
+ Fix language model combobox too small. [Commit.](http://commits.kde.org/kdenlive/7432631c675d034bfbf266443cc2f7ab6774128b)Fixes bug [#465787](https://bugs.kde.org/465787)
+ Scroll timeline when moving a subtitle. Related to #1634. [Commit.](http://commits.kde.org/kdenlive/b53e78e697793e8fff51dc5ef7cc8eb3584147df)
+ Fix subtitles overlap on import. [Commit.](http://commits.kde.org/kdenlive/ffda87a8de2bed6812edc23cc9536a01d3d58f0c)
+ Fix subtitle move regression. [Commit.](http://commits.kde.org/kdenlive/cbc0201a7179834dc63a517919238400bb233d5b)
+ Fix subtitle offset on group move. [Commit.](http://commits.kde.org/kdenlive/56d4646ec9ac56b09c64a7cae39fda98848e927e)
+ Fix subtitles snapping. [Commit.](http://commits.kde.org/kdenlive/c3cfcd9e0254b2b74a5b58d49591bbc08d4c719c)
+ Fix compilation. [Commit.](http://commits.kde.org/kdenlive/7e2421b2aa82e2658a31291f0a769543e3cddada)
+ Fix crash and offset when moving a group with subtitle. [Commit.](http://commits.kde.org/kdenlive/998ddfa5e0471b7dba34ebb16909931cae3d7922)
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Add missing push it. [Commit.](http://commits.kde.org/kdepim-addons/dab416ac2e2ec17bf5a7a9a83064f19a3b04a08c)
+ Make it compile against discount 3. [Commit.](http://commits.kde.org/kdepim-addons/81134b0dff93ecc9da4ae60cd4b0aeb0be95ab1e)
+ Fix crash when message is null. [Commit.](http://commits.kde.org/kdepim-addons/ec5734fe77ca692e882aec88a0801991a375e076)
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Don't check if google resource handler canPerformTask, as this unnecessarily prevents deletion due to lack of item payload. [Commit.](http://commits.kde.org/kdepim-runtime/cfeb68713244eba9e6ce46b8e4c255a2b2b9eea7)
{{< /details >}}
{{< details id="kidentitymanagement" title="kidentitymanagement" link="https://commits.kde.org/kidentitymanagement" >}}
+ Signature editor: Do not specify that the edit file is temporary. [Commit.](http://commits.kde.org/kidentitymanagement/dc2a9a84c5dd65579555b794b8409257784b5119)Fixes bug [#400567](https://bugs.kde.org/400567)
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Mtp: bump dbus timeout to 60 minutes. [Commit.](http://commits.kde.org/kio-extras/5821c30d1f6be18e0993f03db08ec41602e5dc82)Fixes bug [#462059](https://bugs.kde.org/462059)
+ Smb: don't tell the client if the available FS size is 0. [Commit.](http://commits.kde.org/kio-extras/c2c799627aaf440b817fafa96f3662e78c629fe4)See bug [#431050](https://bugs.kde.org/431050)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add ticket.io pkpass extractor script. [Commit.](http://commits.kde.org/kitinerary/6e0b49a8f6577a621fd4a3f13e5b801830197b8e)
+ Look for departure time in boarding pass pkpass files as well. [Commit.](http://commits.kde.org/kitinerary/573dc73f187b9aa08b39e67c3f55b4ceeb838ffa)
+ Fix extracting UK railway ticket numbers. [Commit.](http://commits.kde.org/kitinerary/8ab1e16148cba5cc0e46ef4bdfa5adfef3ac3ebb)
+ Make FlixBus extractor more robust. [Commit.](http://commits.kde.org/kitinerary/0395f3bea2d531d7c9eccad871a3da384e6d8713)
+ Skip test that expects a property added in KCalendarCore 5.103.0. [Commit.](http://commits.kde.org/kitinerary/a376d365f3907e99159e4b63ec31a5fee3bbc699)
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Double-check that we can use basic ranges features. [Commit.](http://commits.kde.org/kleopatra/6b914b164729eae8376f6ee34bbc90682d83cbdd)
{{< /details >}}
{{< details id="kmix" title="kmix" link="https://commits.kde.org/kmix" >}}
+ Appstream: Add default property to the only screenshot. [Commit.](http://commits.kde.org/kmix/fc7691c290aa40dbdceceecdbd10b5119ab13fda)
{{< /details >}}
{{< details id="knights" title="knights" link="https://commits.kde.org/knights" >}}
+ Fix crash loading invalid PGN. [Commit.](http://commits.kde.org/knights/5c79feb19df3f5573bdeaa6098ee6ef76adee7d2)Fixes bug [#465518](https://bugs.kde.org/465518)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Make wasWindowGeometrySaved() function consider new key format. [Commit.](http://commits.kde.org/konsole/7db506b547e159d26fac89ead716548c92f28ca1)
+ Clamp TerminalDisplay size to prevent buffer overflow crash. [Commit.](http://commits.kde.org/konsole/cbc7a4bc41ed1ac45e6c01b444561e4d23809284)
{{< /details >}}
{{< details id="kontact" title="kontact" link="https://commits.kde.org/kontact" >}}
+ Fix bug 464404 :  embedded images not displayed neither as embedded. [Commit.](http://commits.kde.org/kontact/ef1075b9cb7581ca800af1a044ab5f8a8761395d)Fixes bug [#464404](https://bugs.kde.org/464404)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Reparentingmodel.cpp - call layoutAboutToBeChanged. [Commit.](http://commits.kde.org/korganizer/b22e29a16fa640b602ae4095cb59ffdccce1e456)Fixes bug [#462176](https://bugs.kde.org/462176)
+ Create event with range selected in date navigator. [Commit.](http://commits.kde.org/korganizer/a3ee82f497e7091ef10d77794ee3d4822dd53ad0)Fixes bug [#226950](https://bugs.kde.org/226950)
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Try harder to avoid rounding loss in floor level parsing. [Commit.](http://commits.kde.org/kosmindoormap/b0ca512f7f020d8abae5e6be19ac04346b976518)
+ Also apply the layer cap to building parts. [Commit.](http://commits.kde.org/kosmindoormap/0c9df26d1960d08cdd317401d0a4c5218aed526d)
{{< /details >}}
{{< details id="libkleo" title="libkleo" link="https://commits.kde.org/libkleo" >}}
+ Remove duplicated QDebug operator for GpgME::Key. [Commit.](http://commits.kde.org/libkleo/ec13b52ce8a0c2d2fbd9587d2fc91c2e94b1a0c5)
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Don't try to access null dragSource. [Commit.](http://commits.kde.org/lokalize/421cf66c8a3ce615e356212fd6416867cf31a2eb)Fixes bug [#418419](https://bugs.kde.org/418419)
+ Fall back to QDockWidget as an event handler. [Commit.](http://commits.kde.org/lokalize/acf5ff3ba33e46107d478aaa41b80c9fffe11db9)Fixes bug [#465330](https://bugs.kde.org/465330)
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ When we create an empty tab, collection is invalid. [Commit.](http://commits.kde.org/mailcommon/ff4a5a4b463f6c21e3dcb165877ec459d7a445c5)
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Nodehelper.cpp - revert the crash guard change in setNodeDisplayedHidden. [Commit.](http://commits.kde.org/messagelib/b051caa29071604ea50063cd0047b26ff7b23450)
+ Nodehelper.cpp - revert the crash guard change in setNodeProcessed. [Commit.](http://commits.kde.org/messagelib/3437ad360c325c7add262e4a5a12dbf67857b2be)
+ Add guards for invalid nodes. [Commit.](http://commits.kde.org/messagelib/400c615b4b71e243df860bf4e8b7d06686aab778)
+ Don't store empty tab. [Commit.](http://commits.kde.org/messagelib/7696fabe16f778bcdbac2dd384239a372745511f)
{{< /details >}}
{{< details id="pim-sieve-editor" title="pim-sieve-editor" link="https://commits.kde.org/pim-sieve-editor" >}}
+ Appstream: Add default property to the only screenshot. [Commit.](http://commits.kde.org/pim-sieve-editor/f51b7da3c9fd70702fee10654493d2e1ce8d2223)
{{< /details >}}
{{< details id="skanpage" title="skanpage" link="https://commits.kde.org/skanpage" >}}
+ Label adheres to system colouring. [Commit.](http://commits.kde.org/skanpage/78e95c5863f16b29aa3007e83f7b09eead468bc0)
+ Restore printing function. [Commit.](http://commits.kde.org/skanpage/610f9bf3e9bdf36f1196eba8506bf16b08e85652)
+ Fix my previous commit. [Commit.](http://commits.kde.org/skanpage/bd245ce8cf107c51af51d3afa4d13e9c203d670e)
+ Remove redundant options. [Commit.](http://commits.kde.org/skanpage/3008ca0bc7513b48f775e16e3745314e122d7df2)
+ Fix spinboxes' spacing. [Commit.](http://commits.kde.org/skanpage/751236b8f5978ea61fde5bdfd7a1c7d963a0671b)
+ Fix main window theming. [Commit.](http://commits.kde.org/skanpage/bc2dba7d173208e5e210ab2f7388330c41cc9a95)
{{< /details >}}
