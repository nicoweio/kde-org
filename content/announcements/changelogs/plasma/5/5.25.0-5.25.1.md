---
title: Plasma 5.25.1 complete changelog
version: 5.25.1
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Properly center align the compact application delegate. [Commit.](http://commits.kde.org/discover/6048d401aa618ca6af8598b6cde75b01b7bbfa62) 
+ Drop no longer necessary KF5ItemModels dependency. [Commit.](http://commits.kde.org/discover/4f0f295cccf9c8e690fd18b84a26e8e147e28257) 
+ Drop no longer necessary Qt5X11Extras/Qt6GuiPrivate dependency. [Commit.](http://commits.kde.org/discover/fcff5c9d8b17e7c31dff3b6ead7bc3c8a0342894) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Drop bogus KConfigWidgets dep, add explicit KConfig dep. [Commit.](http://commits.kde.org/drkonqi/8bf1aa723ccfdb1dd52ed1a9692cacb3696319c6) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Wallpapers/potd: fix multimonitor support. [Commit.](http://commits.kde.org/kdeplasma-addons/9b27feea0474302be2b475a2ffebf55df4c1533e) Fixes bug [#454333](https://bugs.kde.org/454333)
+ Wallpapers/potd: move providers to providers folder. [Commit.](http://commits.kde.org/kdeplasma-addons/ba22eebae765e176f15a0ef1f43ce7887fe31213) 
+ Drop no longer necessary KF5WindowSystem dependency. [Commit.](http://commits.kde.org/kdeplasma-addons/c225df120480f48afc643e0c73254d6df83c2aef) 
+ [applets/userswitcher] Fix illegal property value. [Commit.](http://commits.kde.org/kdeplasma-addons/f33d16d3746bbcc7e51025e72ee3b536b413db94) 
+ [applets/userswitcher] Use onPressed: wasExpanded... idiom. [Commit.](http://commits.kde.org/kdeplasma-addons/7d76c0761a42bae6c4771109f93d3b09c672ead1) 
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Add `repaintNeeded` to fix warning. [Commit.](http://commits.kde.org/kscreenlocker/b6a9be3bdcf6210a4cff3ced5642499770c3e746) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Effects/desktopgrid: Allow switching between desktops using digit and function keys. [Commit.](http://commits.kde.org/kwin/9591c3a118b830af1864ec622ab1d233db94fbc9) Fixes bug [#455292](https://bugs.kde.org/455292)
+ Unvirtualize Scene::paintWindow(). [Commit.](http://commits.kde.org/kwin/e686fef0bab1a1206fe82aef291e8baa9bb72a9e) 
+ Fix computation of effective opaque region of SurfaceItemX11. [Commit.](http://commits.kde.org/kwin/6f5caa60fe3e33c4850c0bb933a88019da88f2ce) Fixes bug [#455617](https://bugs.kde.org/455617)
+ Fix dragging especially by touch. [Commit.](http://commits.kde.org/kwin/2eb0c67b0bc7ee62e37cb510fafeec66a012b53f) Fixes bug [#455268](https://bugs.kde.org/455268)
+ Fix typo in qml margin. [Commit.](http://commits.kde.org/kwin/22e8b393ae1e02b51faa5dd99e85f6cfbcc517ac) 
+ Backends/drm: do cross-gpu imports again for test commits. [Commit.](http://commits.kde.org/kwin/9d9854b79b7a6eb031695e6190e7bd8bc0bff340) Fixes bug [#454086](https://bugs.kde.org/454086)
+ Effects/windowview: Search on all screens. [Commit.](http://commits.kde.org/kwin/62282a91cc418ab7abf2ac240b3dbb154504f1fe) Fixes bug [#455353](https://bugs.kde.org/455353)
+ Effects: Improve gesture handling in some effects. [Commit.](http://commits.kde.org/kwin/0e2fbaa783d31afba73fdd875a198b795fa14c32) 
+ Qpa: Fix a crash in EGLPlatformContext::swapBuffers(). [Commit.](http://commits.kde.org/kwin/cddfb521e4b69ceb5d78d0b08a7ef020b256ba13) Fixes bug [#455435](https://bugs.kde.org/455435)
+ Backends/drm: use GBM_BO_USE_SCANOUT when importing buffers for multi gpu. [Commit.](http://commits.kde.org/kwin/9baa2c3ee4fef11a5df92fb43fa39ba2ef141709) See bug [#454086](https://bugs.kde.org/454086)
+ Don't use  the plasma theme for icons in overview. [Commit.](http://commits.kde.org/kwin/fd5ee499d200f3a5debc19532626fece79c1a9d5) Fixes bug [#455368](https://bugs.kde.org/455368)
+ Fix windows dragging in desktop grid. [Commit.](http://commits.kde.org/kwin/99529e584b3605c040ad5ad88792a28a2c1ff263) Fixes bug [#455268](https://bugs.kde.org/455268)
+ Effects/windowview: Redirect key events to search field. [Commit.](http://commits.kde.org/kwin/6aed240f880375009dc5759ea1c620842d59d9ef) Fixes bug [#455100](https://bugs.kde.org/455100)
+ [xwl] Restart xwayland regardless of exit value. [Commit.](http://commits.kde.org/kwin/cb04e4d60d547c416da4b094cd5add1373c122f7) 
+ Effects/slide: Ensure that there's only one visibility ref per window. [Commit.](http://commits.kde.org/kwin/21dbc3c973c2deb2768789507e8b40a07b7ac8e0) Fixes bug [#455237](https://bugs.kde.org/455237)
+ Remove redundant initializeX11() in Compositor. [Commit.](http://commits.kde.org/kwin/1274a3d1d8725fa7ca39edd1bc88756aa05ced61) See bug [#455167](https://bugs.kde.org/455167)
+ Always send a done after commit for text input v3. [Commit.](http://commits.kde.org/kwin/7c59ddff59857810a37938c9411d5bd0cfeb2bc3) 
+ Core: Make screen area safer in Workspace::clientArea(). [Commit.](http://commits.kde.org/kwin/80183b359b27973ffbdc8eebaf9882d154d5af55) 
+ Scripting: Handle bad output and desktop ids gracefully. [Commit.](http://commits.kde.org/kwin/5afa5c64409f22ab9e65485780dbb05f9ca47229) See bug [#449957](https://bugs.kde.org/449957)
+ Effects/windowview: do close animation after gesture ended. [Commit.](http://commits.kde.org/kwin/7e1a1dc1552d71b913609681b487a32220b11e09) 
+ Disable screen edge approach area if the edge is blocked. [Commit.](http://commits.kde.org/kwin/35461c014107877e5d530df1239e2800af490f80) Fixes bug [#454503](https://bugs.kde.org/454503)
+ Wayland: Explicitly initialize surface size to 0. [Commit.](http://commits.kde.org/kwin/4adc1cae7edcfceb4f601cd9ea8689b2885375a5) Fixes bug [#454535](https://bugs.kde.org/454535)
+ Fix KWin scripts KCM not writing it's pending states to config. [Commit.](http://commits.kde.org/kwin/8a19796aa676f0e278a9c5c5b7096a9ab2298f0c) Fixes bug [#455015](https://bugs.kde.org/455015)
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ ResultDelegate: Fix action buttons fully expanding on height. [Commit.](http://commits.kde.org/milou/426a779bc01732f6eef4eca90a2a5d735ac9040b) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Add missing i18n. [Commit.](http://commits.kde.org/plasma-desktop/3f4ed0a2c29b04de8a287dec23005f9033b35062) 
+ Desktoppackage: fix unable to apply wallpaper settings after switching layout. [Commit.](http://commits.kde.org/plasma-desktop/82df4b3b5223fa4b31c74d47dbfb0205ab2499c7) Fixes bug [#407619](https://bugs.kde.org/407619)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Fix checkbox not switching when choosing an accent color using color picker. [Commit.](http://commits.kde.org/plasma-workspace/94285f2606d5ec1f6dcfc654d07849c2ba6f9de5) 
+ Kcms/fonts: Fix font hinting preview. [Commit.](http://commits.kde.org/plasma-workspace/e560ea67f0cda92c82774ba776b8c0a72ce4dc43) Fixes bug [#413673](https://bugs.kde.org/413673)
+ Colorsapplicator: make accent-coloured titlebars more robust/foolproof. [Commit.](http://commits.kde.org/plasma-workspace/7e79e056e46d6dc55c5d195dc0ed32125eb7fdc8) 
+ Klipper (classic widget): Fix a system tray menu memory leak. [Commit.](http://commits.kde.org/plasma-workspace/ba12a0901422c68c7bbd6cb74f3a7bde2187ebdc) 
+ Shell/scripting: call `flushPendingConstraintsEvents` in `setLocation`. [Commit.](http://commits.kde.org/plasma-workspace/a5daced302852a79c596ecad7f7868f6c8a3bd8c) 
+ Fix missing index validity check for cursortheme. [Commit.](http://commits.kde.org/plasma-workspace/f219748f5dec01e0bfe9a61bd8de9e6e41bf2afa) Fixes bug [#454829](https://bugs.kde.org/454829)
+ [KSystemActivityDialog] Fix loading default settings. [Commit.](http://commits.kde.org/plasma-workspace/3e652b308fc1135bfcbdc6b811e333f7fe5ac2fc) Fixes bug [#454566](https://bugs.kde.org/454566)
+ Kcms/colors: Fix alternateBackgroundColor for Button not having accents. [Commit.](http://commits.kde.org/plasma-workspace/600b3e747c36c94171f39d857a883a4fc7cc7be5) 
+ Kcms/colors: Ensure that accent color dots always fit on narrow screens. [Commit.](http://commits.kde.org/plasma-workspace/eca44ad8521e661b95f93450fb231a59a4761305) 
{{< /details >}}

{{< details title="plasma-workspace-wallpapers" href="https://commits.kde.org/plasma-workspace-wallpapers" >}}
+ Remove Safe Landing. [Commit.](http://commits.kde.org/plasma-workspace-wallpapers/30a00a67c6e591b9ecf1350995a03d1a61e67369) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Upower: Prevent integer overflow during new brightness computation. [Commit.](http://commits.kde.org/powerdevil/dd74cdbdd3849fbd86e6613ef7ecab6c7857cb89) Fixes bug [#454161](https://bugs.kde.org/454161)
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Connect visibleChanged signal to syncColors. [Commit.](http://commits.kde.org/qqc2-breeze-style/ff601fda3824697c1b4e871abff8c19f6b2dc310) 
+ Revert "PlasmaDesktopTheme: Do not compute the colors for invisible items". [Commit.](http://commits.kde.org/qqc2-breeze-style/248e9b3764eec3c78874b612c8d27126a22facef) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Only show delete button for themes that were manually or through KNS installed. [Commit.](http://commits.kde.org/sddm-kcm/373260064f64486e008b7294e3de41fabe90a9a6) Fixes bug [#454874](https://bugs.kde.org/454874)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Systemsettingsrunner: Fix drag and drop for KCMs using embedded json metadata. [Commit.](http://commits.kde.org/systemsettings/7a04dff6cd3105c7022ed0c18d5464b81aec69b2) 
+ IconMode: Switch to the selected KCM. [Commit.](http://commits.kde.org/systemsettings/92dba7cd9744385535051f43a4614330999945c5) Fixes bug [#444565](https://bugs.kde.org/444565)
{{< /details >}}

