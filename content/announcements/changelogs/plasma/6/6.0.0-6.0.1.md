---
title: Plasma 6.0.1 complete changelog
version: 6.0.1
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Kdecoration: Fix topright corner click when using fractional scaling hack. [Commit.](http://commits.kde.org/breeze/bbb930d3c67e4e2d9bcfb867a8a9a93ffbef005a) Fixes bug [#481857](https://bugs.kde.org/481857)
+ Move additions to left and right int to fix KWin tests. [Commit.](http://commits.kde.org/breeze/f6c8d9aa95feacb798f94b1bad81ea970a802074) 
+ Expand border on bottom and right, set decoration name to "breeze". [Commit.](http://commits.kde.org/breeze/c9558194d929a5a6ad6be5f6fe9b53c2191880bf) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Revert "Round x11GlobalScaleFactor instead of flooring it". [Commit.](http://commits.kde.org/kde-gtk-config/541004a3339fe066d349646684fbb31bfcb564bf) 
{{< /details >}}

{{< details title="KDE Window Decoration Library" href="https://commits.kde.org/kdecoration" >}}
+ Decoration: Only apply hack on windows with outlines. [Commit.](http://commits.kde.org/kdecoration/ef9ae57f6c910fba82109724e10bc7f8fd6dde63) 
+ Push decoration slightly under client if decoration is named "breeze". [Commit.](http://commits.kde.org/kdecoration/d05b511603cf4332a8e25d52a19e792e34d72cdc) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Kwin/effects/cube: Bump clipFar plane. [Commit.](http://commits.kde.org/kdeplasma-addons/cbc126ac29fb2c861af5d81cf5e0be8672b00977) Fixes bug [#482022](https://bugs.kde.org/482022)
+ Kwin/effects/cube: Improve natural scrolling handling. [Commit.](http://commits.kde.org/kdeplasma-addons/9f34a79a82b3162b6a5463b2e656dd08177c91ba) See bug [#482003](https://bugs.kde.org/482003)
+ Kwin/effects/cube: Invert scroll direction. [Commit.](http://commits.kde.org/kdeplasma-addons/c213caa61c2667eee7c262f8d18f4d5d4744a623) Fixes bug [#482003](https://bugs.kde.org/482003)
+ Applets/colorpicker: disable resizing color chooser window. [Commit.](http://commits.kde.org/kdeplasma-addons/79a084e23a025b8fcac2cbe4764a5e16c3ea05d2) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Properly restore "Device" combobox when the output model resets. [Commit.](http://commits.kde.org/kscreen/fa6990ccda3cca3ea202bfb2418fe64fba0209aa) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Effect: Fix EffectWindow::contentsRect(). [Commit.](http://commits.kde.org/kwin/fdcc9be2ce11b04dc423eeb896edcb06571dbc4f) Fixes bug [#482294](https://bugs.kde.org/482294)
+ Fix confined pointer being able to escape the surface. [Commit.](http://commits.kde.org/kwin/7da6ecb3df5edc940eede219dec7efed764e4c22) Fixes bug [#482448](https://bugs.kde.org/482448). See bug [#477124](https://bugs.kde.org/477124)
+ Effects/windowview: use correct enum value for `PointerDevice`. [Commit.](http://commits.kde.org/kwin/63a39646ea088aec71f3e4269bb00dbe7acebad7) Fixes bug [#482191](https://bugs.kde.org/482191)
+ Backends/drm: don't work around pageflips timing out. [Commit.](http://commits.kde.org/kwin/42eca26802d978dcca0924cc6abe4e22121f40be) 
+ Opengl: assume a minimum of 2ms of render time. [Commit.](http://commits.kde.org/kwin/456ed767e6ce30e87829fa11e86cab6586bae7aa) 
+ Backends/drm: fix vblank calculation. [Commit.](http://commits.kde.org/kwin/be9d2fd7d07b84f0a9be7d950947ea097011356f) Fixes bug [#482064](https://bugs.kde.org/482064)
+ Wayland: Fix a crash in DrmLeaseDeviceV1Interface::setDrmMaster(). [Commit.](http://commits.kde.org/kwin/84b4bd3ae8cec346f6d6f35ecd36045ad463d829) 
+ Wayland/surface: don't update preferred color description unless it changed. [Commit.](http://commits.kde.org/kwin/d161f042f6a77d3236d0386765fd5c08561c2399) 
+ Fix ODR violation with MouseClick plugin. [Commit.](http://commits.kde.org/kwin/e89919227f6347ca3696ce6ca16331bf1c8e542b) 
+ Plugins/overview: Adds a border around hovered and selected desktop in desktopGrid. [Commit.](http://commits.kde.org/kwin/565faf1713af149f7adac82834ca1c9574a9c2d2) Fixes bug [#481812](https://bugs.kde.org/481812)
+ Opengl: Harden GLRenderTimeQuery against opengl providing bad timestamps. [Commit.](http://commits.kde.org/kwin/aa741db0e740cfc6df7561e1938fb90fed2b5b68) Fixes bug [#481721](https://bugs.kde.org/481721)
+ Backends/drm: delay cursor updates with adaptive sync. [Commit.](http://commits.kde.org/kwin/27d215357c3dd28bd22fd513f16417e790b1922b) 
+ Backends/drm: move committing logic into a separate method. [Commit.](http://commits.kde.org/kwin/803009848c69ec42c26ce7e9a9ed7819e53897ef) 
+ Backends/x11: Make SwapEventFilter report presentation feedback to OutputFrame. [Commit.](http://commits.kde.org/kwin/63d7a29e8411c177de771cf49c50697084ce4040) 
+ Remove invalid tabbox configs from defaults. [Commit.](http://commits.kde.org/kwin/69b64ef78d5ecd95142a203732f123631a75f77c) Fixes bug [#481640](https://bugs.kde.org/481640)
+ Plugins/outputlocator: show physical size in output locator. [Commit.](http://commits.kde.org/kwin/4145d4a26d12e579c5ef8a49ea5a6af59888bfc2) 
+ Plugins/overview: Search bar can be clicked without closing effect. [Commit.](http://commits.kde.org/kwin/018a1d35db4d799469b82be6c6b7d2551d4bf7d2) 
+ Autotests/test_colorspaces: add an autotest for non-normalized primaries. [Commit.](http://commits.kde.org/kwin/14b96e67e9d20ce6fcc122e90aa63a317ec27b7e) 
+ Core/colorspace: normalize XYZ values before using them in calculations. [Commit.](http://commits.kde.org/kwin/f20ce6244cc2b14145e5c543c6b8d5fdbb36bd66) Fixes bug [#481034](https://bugs.kde.org/481034)
+ Comopsitor: only activate VRR if the active window is on the current screen. [Commit.](http://commits.kde.org/kwin/75b741128ebd662e832dae333a683dbd3a5317de) Fixes bug [#481750](https://bugs.kde.org/481750)
+ Xwayland: Ignore somebody else claiming WM_S0 selection. [Commit.](http://commits.kde.org/kwin/5b263b41d5326109351f280430d21b8880509d5b) 
+ Fix a crash in eglDestroyImageKHR(). [Commit.](http://commits.kde.org/kwin/4ae0f0cd654ce3e2a4d885062bddf612f7579f97) Fixes bug [#470980](https://bugs.kde.org/470980)
+ Window: Don't reset quick tile mode on interactive move start. [Commit.](http://commits.kde.org/kwin/7a3693867284cc72932ee8901e7e79a3681aee13) Fixes bug [#472366](https://bugs.kde.org/472366)
+ X11window: Disable strict geometry placement by default in wayland. [Commit.](http://commits.kde.org/kwin/ebce0e3c3371fbc5a59955c91873edca1b6e4e79) Fixes bug [#481456](https://bugs.kde.org/481456)
+ Tileseditor: Don't allow tiles to move tiles already at minimum size. [Commit.](http://commits.kde.org/kwin/bd71e4832725afa2adc156d15b2cd4589d7e36f2) Fixes bug [#465721](https://bugs.kde.org/465721)
+ Wayland: Install the clientconnection.h header file. [Commit.](http://commits.kde.org/kwin/fbe35105acb5edc6a4a34bd710684c4265fef888) 
+ Useractions: don't interact with deleted windows. [Commit.](http://commits.kde.org/kwin/10eac23ed2941c1ce508bfccd57825164c3ca1e4) Fixes bug [#481688](https://bugs.kde.org/481688)
+ Plugins/showpaint: Add support for color management. [Commit.](http://commits.kde.org/kwin/66af39ec9e46431ea7a4577ca1f5be5d6a890691) 
+ Plugins/glide: Fix rotation order when applying render target transformation. [Commit.](http://commits.kde.org/kwin/515f60cb7f94b01311a81360a5884409eb2199f3) Fixes bug [#481664](https://bugs.kde.org/481664)
+ Send tablet input to active screen. [Commit.](http://commits.kde.org/kwin/111cd842f1d41c1ee0c9297eb2c796311433883c) Fixes bug [#479713](https://bugs.kde.org/479713)
+ Backends/x11: Avoid calling doneCurrent() if GLX context has not been created. [Commit.](http://commits.kde.org/kwin/df4b12550680e8a2a6bf34bf13e932e5b815159c) Fixes bug [#477854](https://bugs.kde.org/477854)
+ Backends/wayland: Guard against failing to create EGLSwapchain. [Commit.](http://commits.kde.org/kwin/cb3dadf283fd7952a649b6d079ba244b74fd2bec) Fixes bug [#478864](https://bugs.kde.org/478864)
+ Backends/drm: only enable HDR if both display and driver are capable of it. [Commit.](http://commits.kde.org/kwin/80e30821af7b1675f76dbcccf74eac4b61ff58d0) Fixes bug [#481518](https://bugs.kde.org/481518)
+ Xcbutils: arm x apps can't be dragged to a negative position on the screen. [Commit.](http://commits.kde.org/kwin/2f4480e60b3cac686642bad555ce5162a5dfad75) 
+ Wayland: Avoid rearranging layer surfaces when wl_surface size changes. [Commit.](http://commits.kde.org/kwin/47973b4f1cf0e720a969621b67847909163a204e) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Handle the translations for systemstats. [Commit.](http://commits.kde.org/libksysguard/815a5b43bf343d6497cbc0cd43aa931bb7e78e0c) 
{{< /details >}}

{{< details title="libplasma" href="https://commits.kde.org/libplasma" >}}
+ Show a more specific error message for incompatible widgets. [Commit.](http://commits.kde.org/libplasma/231beaedc2a2a03e304aa5b59d3707ff5d59b99b) Fixes bug [#481328](https://bugs.kde.org/481328)
+ Don't show arrow for icon-only buttons. [Commit.](http://commits.kde.org/libplasma/8ea86c22ea190d87f69890e21fd6fc677a4d5933) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Keep first selected item if currentIndex is 0. [Commit.](http://commits.kde.org/plasma-desktop/6212ff4439e4033d44aad5c0f5665e4dfcd2b7e5) 
+ Fix kicker close on click away. [Commit.](http://commits.kde.org/plasma-desktop/15f704c84a2280a7312680ac21a975e7f77b1012) Fixes bug [#482324](https://bugs.kde.org/482324)
+ Fix some warnings. [Commit.](http://commits.kde.org/plasma-desktop/664b09347c45ca5da529d92e2e72315d089de0bf) 
+ Disable internal Key navigation. [Commit.](http://commits.kde.org/plasma-desktop/4a6ef6061104e31fcc6ddb9c4e6f970856d2fe89) Fixes bug [#482005](https://bugs.kde.org/482005)
+ AppletError: Use compact error message from libplasma as the header. [Commit.](http://commits.kde.org/plasma-desktop/9d3ed348b654143d91230cd1d3e1d54ff7139fa5) 
+ 🍒Fix default appstream handler. [Commit.](http://commits.kde.org/plasma-desktop/54506e2b1405f5b5e8dade766b0ad63355e88f74) Fixes bug [#481932](https://bugs.kde.org/481932)
+ Appiumtests: move desktop tests to its own folder. [Commit.](http://commits.kde.org/plasma-desktop/2630150b85224f594ff2a5ae04de59fa4f990760) 
+ Fix running appstreamtest. [Commit.](http://commits.kde.org/plasma-desktop/658759f5551591451c5ca534d477f9ddcbf7e1ee) 
+ Kcms:keys: fix changing command text or path. [Commit.](http://commits.kde.org/plasma-desktop/7f70e727f1c54b1b305f4f6c510c1e947066013e) Fixes bug [#481825](https://bugs.kde.org/481825)
+ FolderView Positioner: fix rows insert and remove. [Commit.](http://commits.kde.org/plasma-desktop/e306c63ddbeaccea72a03fbb2c3dbd8245addef5) Fixes bug [#481254](https://bugs.kde.org/481254). Fixes bug [#466337](https://bugs.kde.org/466337)
+ Applets/taskmanager: simplify right press handling. [Commit.](http://commits.kde.org/plasma-desktop/a2c763a16916dd154216174d9bf459c743f5e5d9) 
+ Revert "Fix i18n domain". [Commit.](http://commits.kde.org/plasma-desktop/459ade071534de37f29bb1eedbacdb7b59f05f82) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Kwin/convergentwindows: Don't set maximize property for fullscreen windows. [Commit.](http://commits.kde.org/plasma-mobile/5290f5e609953f0ca0d9b7ffdbbc6a787bcec457) 
+ Meta: Fix appstream tests. [Commit.](http://commits.kde.org/plasma-mobile/b4f55f6803dd70fb45bd2adf6e46ae70c0e9e39d) 
+ Meta: Add tag to posix test. [Commit.](http://commits.kde.org/plasma-mobile/83939809d7fca24304e15f6101340cefb4b4c6e4) 
+ Taskswitcher: Remove all interaction restrictions while the task switcher is opening. [Commit.](http://commits.kde.org/plasma-mobile/79d0036a44709e8e92182bf060c27c0389280852) 
+ Kwin/convergentwindows: Fix race condition causing windows to be maximized too large. [Commit.](http://commits.kde.org/plasma-mobile/dc55dafe6607be83be7003acc83ddcc766aaa0dc) 
+ Taskpanel: Delay setting panel position on screen change, so shell doesn't crash. [Commit.](http://commits.kde.org/plasma-mobile/6e674d68b5cc2b245c7ca66bc74421fff0efd801) 
+ Homescreens/folio: Don't show background for widget configure dialog. [Commit.](http://commits.kde.org/plasma-mobile/7135ce07769c084e0c916f52d5adc4d576e501bc) 
{{< /details >}}

{{< details title="plasma-welcome" href="https://commits.kde.org/plasma-welcome" >}}
+ Supporters: Update list of supporters. [Commit.](http://commits.kde.org/plasma-welcome/458d92e27f09d9ab35dd3746587f472a5150cbac) 
+ Remove unnecessary .arg call from release URL. [Commit.](http://commits.kde.org/plasma-welcome/45c9b81593029ac4f222fda1045e43e9b377e310) 
+ Correct release URL for Plasma 6.0. [Commit.](http://commits.kde.org/plasma-welcome/4b7b0d83b3411177e6b9e0bfb7b7d4bf91321dae) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Applets/systemtray: Prefer Icon over IconName. [Commit.](http://commits.kde.org/plasma-workspace/976575921d2a1bbd2551afba335be08ebf0f2a25) Fixes bug [#479712](https://bugs.kde.org/479712)
+ Make sure AppItem is up to date with sycoca when checking isValid(). [Commit.](http://commits.kde.org/plasma-workspace/b5ce9cb4db74a54fbed4e10f4fe7cf656b899b9d) Fixes bug [#481855](https://bugs.kde.org/481855)
+ Kcms/nightcolor: Fix DayNightView on edge cases. [Commit.](http://commits.kde.org/plasma-workspace/096149c39fac116d5c392830b361641b9eaf9ac4) Fixes bug [#482032](https://bugs.kde.org/482032)
+ Applets: add missing website in metadata. [Commit.](http://commits.kde.org/plasma-workspace/a9d11354a6bffff22fa15c862a6e580f1da7a704) 
+ Libkmpris: find active player index in filter proxy model. [Commit.](http://commits.kde.org/plasma-workspace/89f68ceccd08bda6f804a9b1c9c722ef0852d40b) Fixes bug [#481992](https://bugs.kde.org/481992)
+ Libkmpris: connect to signals from base class. [Commit.](http://commits.kde.org/plasma-workspace/b74128601e2f9edb31ad19b99eecbe4b2f73e32e) 
+ (cherry picked from commit e605a723c98a9aa1b48fcaec25c34aa20e5430e6). [Commit.](http://commits.kde.org/plasma-workspace/d9a2dc413c67890687c79b4d77a4f07124efdad0) 
+ Appiumtests: add test for SystemDialog. [Commit.](http://commits.kde.org/plasma-workspace/4940a23b184bb516d26ed3f61d390ca55659ef3b) 
+ Show panels on entering edit mode and "add widgets". [Commit.](http://commits.kde.org/plasma-workspace/8fc5105fec06e80cd2e6d1a3541bf0d8211d9c9f) Fixes bug [#448393](https://bugs.kde.org/448393)
+ Notifications: Only convert absolute paths to file URLs. [Commit.](http://commits.kde.org/plasma-workspace/403431b1b801326cbca732dbcc759a719c1bb7f0) Fixes bug [#481759](https://bugs.kde.org/481759)
+ Applets/cameraindicator: add bug report button. [Commit.](http://commits.kde.org/plasma-workspace/5b0fb6b8ef264c9acdfdebb43c6c67edb73f43d3) 
+ Revert "panelview: Take focus from other Plasma windows when clicked". [Commit.](http://commits.kde.org/plasma-workspace/d09d50c3664efacf3bf8c7f2effe338a05cb816c) Fixes bug [#481671](https://bugs.kde.org/481671). See bug [#367815](https://bugs.kde.org/367815)
+ Components/dialogs: fall back to null target when dialogButtonBox is null. [Commit.](http://commits.kde.org/plasma-workspace/58ef0c40b6fc098f9a02f816fc0470177a0d8f70) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ [dimdisplay] Respect profile configurations. [Commit.](http://commits.kde.org/powerdevil/8681355217eb6ff121785572eaf4ef2ca21b464b) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Remove overriding of Kirigami application header. [Commit.](http://commits.kde.org/qqc2-breeze-style/f04a73f55db2a4dc8391eca28ca741ffeaeaf20c) 
+ ScrollView: Improve scrolling behaviour. [Commit.](http://commits.kde.org/qqc2-breeze-style/523040c68ae248a7c0f15ff5ed58d893e8062dd1) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Runner: Improve granularity of relevance and category relevance. [Commit.](http://commits.kde.org/systemsettings/ffe063e781140119918f6120285ab93edace47e2) 
+ Improve page separator in header. [Commit.](http://commits.kde.org/systemsettings/6b636e5bb83f79e3d7f41fc5f1aa7a8ffc06690b) See bug [#435481](https://bugs.kde.org/435481)
{{< /details >}}
