Dir: kdeedu
----------------------------
new version numbers



Dir: kdeedu/debian
----------------------------
Update menu files for 3.3.
----------------------------
Updated INDI manpages.
----------------------------
Oops.
----------------------------
More files moving around.
----------------------------
And again.
----------------------------
Updated doc-base/etc for 3.3.
----------------------------
Update author information.
----------------------------
Fix source overrides (see #261435).
----------------------------
New binary packages, rearrange install files, update shlibs.
----------------------------
More author and license updates.
----------------------------
Updates for 3.3.1.
----------------------------
Added manpages for new apps.
----------------------------
Updated descriptions for existing manpages.



Dir: kdeedu/kalzium/src
----------------------------
backport
----------------------------
backport fix for 91287
----------------------------
compile



Dir: kdeedu/kbruch/src
----------------------------
fix bug 92482: input box is now grayed out
----------------------------
fix bug 93080: some digits were missing in the numbers to be converted, because just a default precision of 2 was used in KLocale::formatNumber(...)



Dir: kdeedu/kiten
----------------------------
don't link a C application against Qt
----------------------------
fix clear in radical search
CCBUG:87050
----------------------------
backport 93414



Dir: kdeedu/klatin/klatin
----------------------------
Requested by Krzysztof Lichota



Dir: kdeedu/kmathtool
----------------------------
Here too: too many escapes (Sorry!)



Dir: kdeedu/kmplot/kmplot
----------------------------
Fix for bug 92014: program hangs when plotting arcsin and arccos.

BUG: 92014
