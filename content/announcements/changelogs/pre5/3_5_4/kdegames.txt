2006-06-02 23:47 +0000 [r547684]  staikos

	* branches/KDE/3.5/kdegames/kbackgammon/kbackgammonui.rc: don't
	  show a feature that doesn't exist

2006-06-10 15:08 +0000 [r549992]  lueck

	* branches/KDE/3.5/kdegames/doc/ktuberling/technical-reference.docbook,
	  branches/KDE/3.5/kdegames/doc/ktuberling/index.docbook:
	  documentation backport from trunk CCMAIL:kde-doc-english@kde.org
	  CCMAIL:kde-i18n-doc@kde.org

2006-06-11 18:13 +0000 [r550426]  davec

	* branches/KDE/3.5/kdegames/kshisen/board.cpp,
	  branches/KDE/3.5/kdegames/kshisen/board.h: Patch from Olivier
	  Trichet to fix bug #127474.

2006-06-17 11:05 +0000 [r552293]  aacid

	* branches/KDE/3.5/kdegames/kmahjongg/Editor.cpp: Fix board editor
	  has garbage when opening it. Add a bit of margin so that Qt does
	  not decide to omit calling Editor::paintEvent (bad Qt changing
	  your behaviour!) This is a bit of a dirty trick but it's the less
	  intrusive patch i could find that fixed the issue BUGS: 129280

2006-06-17 11:51 +0000 [r552305]  aacid

	* branches/KDE/3.5/kdegames/kmahjongg/Editor.cpp: Don't tell the
	  user only saving to local medium is supported if he clicks cancel
	  BUGS: 129284

2006-06-18 08:19 +0000 [r552543]  mlaurent

	* branches/KDE/3.5/kdegames/kgoldrunner/src/kgrdialog.cpp: Fix
	  missing i18n

2006-06-20 13:18 +0000 [r553220]  lueck

	* branches/KDE/3.5/kdegames/kreversi/qreversigameview.cpp: fixed
	  untranslatable string -> missing i18n() call
	  CCMAIL:kde-i18n-doc@kde.org CCMAIL:inge@lysator.liu.se

2006-06-23 12:40 +0000 [r554178]  swinter

	* branches/KDE/3.5/kdegames/kolf/newgame.cpp: changing broken link
	  to prevent redirection to sports bets page still some other
	  references to email addresses at katzbrown.com in the code
	  CCMAIL:jasonkb@mit.edu

2006-06-28 16:31 +0000 [r555873]  coolo

	* branches/KDE/3.5/kdegames/kpat/dealer.cpp: applying sent patch to
	  reenable hint after game was lost and undoed

2006-07-02 19:20 +0000 [r557254]  ingwa

	* branches/KDE/3.5/kdegames/kreversi/qreversigameview.cpp,
	  branches/KDE/3.5/kdegames/kreversi/ChangeLog: Fix bug 129488:
	  kreversi: wrong player (=color) names in moves window Write
	  Red/Blue in addition to White/Black if the user has chosen non-BW
	  in the preferences. BUG: 129488

2006-07-03 13:41 +0000 [r557537-557535]  ingwa

	* branches/KDE/3.5/kdegames/kreversi/qreversigameview.h,
	  branches/KDE/3.5/kdegames/kreversi/Position.cpp,
	  branches/KDE/3.5/kdegames/kreversi/ChangeLog: Fix bug 127531:
	  kreversi does not update score on undo. - Keep track of score
	  when removing a move too. BUG: 127531

	* branches/KDE/3.5/kdegames/kreversi/version.h,
	  branches/KDE/3.5/kdegames/kreversi/ChangeLog: Update version
	  number to 1.7.1 for KDE 3.5.4.

2006-07-03 14:30 +0000 [r557558]  ingwa

	* branches/KDE/3.5/kdegames/kreversi/kreversi.cpp,
	  branches/KDE/3.5/kdegames/kreversi/ChangeLog: Fix bug 110942: The
	  hourglass cursor is kept too long - Set state to Ready when the
	  game is over. BUG: 110942

2006-07-09 11:59 +0000 [r560146]  lueck

	* branches/KDE/3.5/kdegames/doc/kbackgammon/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kreversi/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kpoker/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kwin4/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ktron/index.docbook,
	  branches/KDE/3.5/kdegames/doc/klickety/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kshisen/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kmahjongg/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kfouleggs/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ksokoban/index.docbook:
	  documentation backport from trunk CCMAIL:kde-doc-english

2006-07-17 12:00 +0000 [r563361]  bero

	* branches/KDE/3.5/kdegames/libkdegames/kgame/kgamepropertylist.h:
	  gcc 4.x fixes noted while compiling ksirk

2006-07-22 12:54 +0000 [r565111]  amantia

	* branches/KDE/3.5/kdegames/libksirtet/common/ai.h,
	  branches/KDE/3.5/kdegames/libksirtet/lib/libksirtet_export.h
	  (added),
	  branches/KDE/3.5/kdegames/libksirtet/common/highscores.h,
	  branches/KDE/3.5/kdegames/libksirtet/common/types.cpp,
	  branches/KDE/3.5/kdegames/libksirtet/common/settings.h,
	  branches/KDE/3.5/kdegames/libksirtet/base/gtetris.h,
	  branches/KDE/3.5/kdegames/libksirtet/common/types.h,
	  branches/KDE/3.5/kdegames/libksirtet/base/field.h,
	  branches/KDE/3.5/kdegames/libksirtet/common/factory.h,
	  branches/KDE/3.5/kdegames/libksirtet/base/board.h,
	  branches/KDE/3.5/kdegames/libksirtet/lib/mp_simple_board.h,
	  branches/KDE/3.5/kdegames/libksirtet/base/kzoommainwindow.h,
	  branches/KDE/3.5/kdegames/libksirtet/base/highscores.h,
	  branches/KDE/3.5/kdegames/libksirtet/common/inter.h,
	  branches/KDE/3.5/kdegames/libksirtet/common/field.h,
	  branches/KDE/3.5/kdegames/libksirtet/common/main.h,
	  branches/KDE/3.5/kdegames/libksirtet/common/board.h,
	  branches/KDE/3.5/kdegames/libksirtet/common/misc_ui.h: Visibility
	  fixes.

2006-07-22 18:40 +0000 [r565221]  aacid

	* branches/KDE/3.5/kdegames/kmahjongg/version.h: version++

2006-07-23 14:02 +0000 [r565470]  coolo

	* branches/KDE/3.5/kdegames/kdegames.lsm: preparing KDE 3.5.4

