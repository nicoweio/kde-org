------------------------------------------------------------------------
r953913 | rdale | 2009-04-14 18:36:35 +0000 (Tue, 14 Apr 2009) | 2 lines

* Promote some changes from the trunk to the 4.2 release branch

------------------------------------------------------------------------
r960565 | rdale | 2009-04-28 16:48:41 +0000 (Tue, 28 Apr 2009) | 2 lines

* Promote solid enum fix to the release branch

------------------------------------------------------------------------
r960566 | rdale | 2009-04-28 16:49:28 +0000 (Tue, 28 Apr 2009) | 4 lines

* Promote QtRuby 2.0.4 to the release branch

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r960967 | rdale | 2009-04-29 11:31:30 +0000 (Wed, 29 Apr 2009) | 2 lines

* Promote fix for conversion operators to the release branch

------------------------------------------------------------------------
r960969 | rdale | 2009-04-29 11:33:16 +0000 (Wed, 29 Apr 2009) | 2 lines

* Add the KIconCache and KPixmapCache classes

------------------------------------------------------------------------
r960971 | rdale | 2009-04-29 11:34:19 +0000 (Wed, 29 Apr 2009) | 2 lines

* The qttest extension doesn't need to link against the smokekde lib

------------------------------------------------------------------------
