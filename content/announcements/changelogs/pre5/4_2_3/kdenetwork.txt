------------------------------------------------------------------------
r945174 | mattr | 2009-03-27 02:52:29 +0000 (Fri, 27 Mar 2009) | 1 line

handle correctly PGP encrypted messages
------------------------------------------------------------------------
r945175 | mattr | 2009-03-27 02:52:35 +0000 (Fri, 27 Mar 2009) | 64 lines

aim chatroom join fix and (receiving) invitation support

Squashed commit of the following:

commit 00cd5955bc4d789b9c18f251d0748f4d37926052
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Tue Mar 24 23:51:02 2009 -0400

    changed private member name

commit fbc6ef6ed76c9e11c73565cf48b4a569797ec3d1
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Tue Mar 24 00:55:34 2009 -0400

    spacing

commit dddae54107b4dcdf556e0218332cfbdc347ad538
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Sun Mar 22 23:00:22 2009 -0400

    changed dialog to modeless

commit c3037ebf9e93e9e9014951e5623a6bab7290f054
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Sun Mar 22 16:58:52 2009 -0400

    some reformatting
    added reject response

commit 91c1be1422345244963da9f11e7118e41c3a3ddb
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Sun Mar 22 14:07:37 2009 -0400

    added title

commit b31bb4698973bfbfe36e9962dc06c52642b3a717
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Sun Mar 22 12:44:12 2009 -0400

    copyright stuff

commit adc00e5caf80bfcd7ccd13ab097596f51695c0eb
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Sun Mar 22 12:41:07 2009 -0400

    fixed spacing

commit 613ef20dfb3f4d9eaf0ea346ff9026e083c9ac1e
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Sun Mar 22 12:40:21 2009 -0400

    made sending messages in a chat room work =D

commit d4a6210dda464d213f338a8bd9f4aeddf5481502
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Sun Mar 22 12:01:49 2009 -0400

    a chat window opens now!

commit b0314778ea423b2db93d016ac4573d5aca5888b4
Author: Benson Tsai <btsai@vrwarp.com>
Date:   Sun Mar 22 03:57:07 2009 -0400

    beginning chat invitation support
------------------------------------------------------------------------
r945176 | mattr | 2009-03-27 02:52:40 +0000 (Fri, 27 Mar 2009) | 1 line

- do not publish all the MSN client protocols in the clientId, only the last one.
------------------------------------------------------------------------
r946335 | chehrlic | 2009-03-29 09:01:34 +0000 (Sun, 29 Mar 2009) | 1 line

win32 compile++
------------------------------------------------------------------------
r946720 | scripty | 2009-03-30 07:09:12 +0000 (Mon, 30 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r947653 | mattr | 2009-04-01 03:00:44 +0000 (Wed, 01 Apr 2009) | 3 lines

Fix setting v4l2 controls with minimum values other than 0

Patch by Brian Johnson. Thanks!
------------------------------------------------------------------------
r948021 | gberg | 2009-04-02 00:14:39 +0000 (Thu, 02 Apr 2009) | 3 lines

Remove the telepathy protocol plugin. Its broken and deprecated and gone in trunk.

(Backport of commit 947928)
------------------------------------------------------------------------
r949078 | kbal | 2009-04-04 13:22:42 +0000 (Sat, 04 Apr 2009) | 1 line

show icon
------------------------------------------------------------------------
r949088 | schafer | 2009-04-04 13:45:00 +0000 (Sat, 04 Apr 2009) | 3 lines

Fixed bug that causes a crash if the GLOBAL section is missing in the smb.conf file
BUG: 99376

------------------------------------------------------------------------
r949233 | schafer | 2009-04-04 17:04:11 +0000 (Sat, 04 Apr 2009) | 4 lines

Fixed a crash that was caused, when options set in the smb.conf do
not exist in the corresponding ComboBoxes of the GUI.
BUG: 166106

------------------------------------------------------------------------
r949881 | scripty | 2009-04-06 07:19:57 +0000 (Mon, 06 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r950256 | ogoffart | 2009-04-06 17:26:45 +0000 (Mon, 06 Apr 2009) | 4 lines

Backport:  950068 
Automatically show the notification messages (green flags)  so people do not miss it.  


------------------------------------------------------------------------
r951670 | djarvie | 2009-04-09 21:10:21 +0000 (Thu, 09 Apr 2009) | 2 lines

Make it build (by removing deprecated calls)

------------------------------------------------------------------------
r951957 | mfuchs | 2009-04-10 16:40:50 +0000 (Fri, 10 Apr 2009) | 3 lines

Backport r951854
Filtering works correctly now.

------------------------------------------------------------------------
r952115 | mfuchs | 2009-04-10 23:24:07 +0000 (Fri, 10 Apr 2009) | 2 lines

Fix a regression introduced with the previous commit. Downloading imported links works again.

------------------------------------------------------------------------
r952119 | mfuchs | 2009-04-10 23:40:25 +0000 (Fri, 10 Apr 2009) | 5 lines

Backport -r952118
Enabling Konqueror integration on first start up also takes effect
in the systray context-menu.
CCBUG:179844

------------------------------------------------------------------------
r952931 | pino | 2009-04-12 21:50:22 +0000 (Sun, 12 Apr 2009) | 2 lines

extra ','

------------------------------------------------------------------------
r954141 | segato | 2009-04-15 08:41:57 +0000 (Wed, 15 Apr 2009) | 3 lines

close the session if the view is disconnected
BUG:188733

------------------------------------------------------------------------
r955078 | bram | 2009-04-16 21:48:07 +0000 (Thu, 16 Apr 2009) | 5 lines

Custom notifications for contacts work again.

Patch by Lamarque Vieira Souza, thank you!

BUG:175687
------------------------------------------------------------------------
r956014 | uwolfer | 2009-04-19 09:23:38 +0000 (Sun, 19 Apr 2009) | 4 lines

Backport:
SVN commit 956008 by uwolfer:

Fix issue that there were dead tooltops flying around when automatically reloading the downloads list.
------------------------------------------------------------------------
r957964 | scripty | 2009-04-23 07:33:00 +0000 (Thu, 23 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r961126 | rjarosz | 2009-04-29 14:38:42 +0000 (Wed, 29 Apr 2009) | 3 lines

Don't show html tags in chat.


------------------------------------------------------------------------
