------------------------------------------------------------------------
r1055585 | mzanetti | 2009-11-28 11:02:54 +0000 (Sat, 28 Nov 2009) | 3 lines

use ArgumentDelegate also in AddAction
BUG: 216050

------------------------------------------------------------------------
r1063316 | kossebau | 2009-12-17 20:08:12 +0000 (Thu, 17 Dec 2009) | 2 lines

backport of #1063313: fixed: default login name always was an empty string, not the one of the user on this system (isEmpty vs. isNull problem)'

------------------------------------------------------------------------
r1066857 | scripty | 2009-12-28 04:21:51 +0000 (Mon, 28 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1067162 | rkcosta | 2009-12-29 02:30:07 +0000 (Tue, 29 Dec 2009) | 9 lines

Backport r1067160.

Treat the preview dialog as a non-modal dialog.

There was no point in calling exec() here, just call show() and be
happy.

CCBUG: 216601

------------------------------------------------------------------------
r1067164 | rkcosta | 2009-12-29 03:04:52 +0000 (Tue, 29 Dec 2009) | 6 lines

Backport r1067163.

Add support for the application/x-xz-compressed-tar mimetype.

CCBUG: 218929

------------------------------------------------------------------------
r1067169 | scripty | 2009-12-29 04:16:38 +0000 (Tue, 29 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1067181 | rkcosta | 2009-12-29 05:01:20 +0000 (Tue, 29 Dec 2009) | 4 lines

Backport r1067177.

Simplify the code a little bit.

------------------------------------------------------------------------
r1067183 | rkcosta | 2009-12-29 05:08:10 +0000 (Tue, 29 Dec 2009) | 10 lines

Backport r1067179.

Do not call QDir::relativeFilePath() from QDir::current().

QDir::setCurrent() is called if global work dir is set, which ends
up resolving symlinks automatically. Creating a QDir and calling
relativeFilePath() on this object solves the problem.

CCBUG: 191821

------------------------------------------------------------------------
r1068794 | kossebau | 2010-01-02 01:35:18 +0000 (Sat, 02 Jan 2010) | 1 line

backport of 1068786: fixed: using the current coding from the view was accidently inactivated, so always the default hexadecimal was used
------------------------------------------------------------------------
r1069173 | rkcosta | 2010-01-02 20:54:41 +0000 (Sat, 02 Jan 2010) | 10 lines

Backport r1069172.

Add the "could not find volume" error message to the possible rar error
messages.

Also detect error messages when listing. The error messages must be improved,
though.

CCBUG: 208047

------------------------------------------------------------------------
r1069574 | rkcosta | 2010-01-03 19:51:00 +0000 (Sun, 03 Jan 2010) | 4 lines

Backport r1069572.

Make the accessor methods const.

------------------------------------------------------------------------
r1072378 | scripty | 2010-01-10 04:10:14 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1073658 | rkcosta | 2010-01-12 16:20:30 +0000 (Tue, 12 Jan 2010) | 8 lines

Backport r1073654 in case 4.3.5 ends up being released.

Show the date and time for each file in a zip archive.

The regexp for each line has been improved a little in the process ;)

CCBUG: 222398

------------------------------------------------------------------------
r1074244 | dakon | 2010-01-13 17:54:42 +0000 (Wed, 13 Jan 2010) | 4 lines

set correct object parent

CCBUG:222521

------------------------------------------------------------------------
r1074903 | rkcosta | 2010-01-14 22:43:08 +0000 (Thu, 14 Jan 2010) | 6 lines

Backport r1074887 to 4.3.

Add application/x-lzma-compressed-tar (tar.lzma/tlz) to the supported formats list.

CCBUG: 222779

------------------------------------------------------------------------
r1076370 | scripty | 2010-01-18 04:09:04 +0000 (Mon, 18 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
