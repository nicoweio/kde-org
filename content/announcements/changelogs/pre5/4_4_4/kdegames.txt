------------------------------------------------------------------------
r1121275 | scripty | 2010-05-01 13:50:46 +1200 (Sat, 01 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1123623 | schwarzer | 2010-05-07 00:36:58 +1200 (Fri, 07 May 2010) | 7 lines

backport of r1116270: swap rotating directions

Enter and right-click should rotate clock-wise
Ctrl+Enter and left-click counter clock-wise

BUG: 170066

------------------------------------------------------------------------
r1123625 | schwarzer | 2010-05-07 00:41:45 +1200 (Fri, 07 May 2010) | 11 lines

backport of r1116276 and r1116513: Dynamically calculate the maximum number of mines in the custom config.

The maximum number of mines that KMines actually allows on a custom
board is WIDTH * HEIGHT - 10. Instead of hardcoding any particular
maximum, the dialog now dynamically sets the maximum value of the
mines spinbox everytime the values of the width and height spinboxes
changed.

BUG: 163806


------------------------------------------------------------------------
r1123626 | schwarzer | 2010-05-07 00:43:06 +1200 (Fri, 07 May 2010) | 5 lines

backport of r1116649: allow more than 99 wins to be shown

BUG: 172930


------------------------------------------------------------------------
r1123628 | schwarzer | 2010-05-07 00:48:01 +1200 (Fri, 07 May 2010) | 3 lines

backport of r1116258: Remember the state of the toolbar and statusbar between sessions.


------------------------------------------------------------------------
r1123632 | schwarzer | 2010-05-07 00:50:59 +1200 (Fri, 07 May 2010) | 4 lines

backport of r1118003: fix ace of spades in dondorf carddeck

BUG: 180186

------------------------------------------------------------------------
r1124278 | ducroquet | 2010-05-09 03:12:10 +1200 (Sun, 09 May 2010) | 1 line

Backport fix for bug #235768 in 4.4 branch.
------------------------------------------------------------------------
r1126468 | scripty | 2010-05-14 14:03:19 +1200 (Fri, 14 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1127585 | scripty | 2010-05-17 14:17:07 +1200 (Mon, 17 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1130689 | scripty | 2010-05-26 14:09:59 +1200 (Wed, 26 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1130917 | majewsky | 2010-05-27 06:58:15 +1200 (Thu, 27 May 2010) | 4 lines

BUG: 238843

Yay! In time for KDE SC 4.4.4.

------------------------------------------------------------------------
