---
aliases:
- ../announce-frameworks5-alpha2
date: 2014-03-03
description: KDE Ships Second Alpha of Frameworks 5
title: KDE Ships Second Alpha of Frameworks 5
---

{{<figure src="/announcements/frameworks/5-tp/KDE_QT.jpg" class="text-center" caption="Collaboration between Qt and KDE" >}}

Today KDE released the second alpha of Frameworks 5, part of a series of releases leading up to the final version planned for June 2014. This release includes progress since the <a href='../5-alpha'>previous alpha</a> in February.

Efforts were directed towards getting frameworks to work on the MacOSX platform and <a href='http://blog.martin-graesslin.com/blog/2014/02/running-frameworks-powered-applications-on-wayland/'>Wayland on Linux</a>. Moreover, kprintutils is no more, kwallet-framework was renamed kwallet and some new frameworks have been added.

For information about Frameworks 5, see <a href='http://dot.kde.org/2013/09/25/frameworks-5'>this article on the dot news site</a>. Those interested in following progress can check out the <a href='https://projects.kde.org/projects/frameworks'>git repositories</a>, follow the discussions on the <a href='https://mail.kde.org/mailman/listinfo/kde-frameworks-devel'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='https://git.reviewboard.kde.org/groups/kdeframeworks/'>review board</a>. Policies and the current state of the project and plans are available at the <a href='http://community.kde.org/Frameworks'>Frameworks wiki</a>. Real-time discussions take place on the <a href="irc://irc.libera.chat/#kde-devel">#kde-devel IRC channel on libera.chat</a>.

## Discuss, Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for these releases of KDE software.

You can discuss this news story <a href='http://dot.kde.org/2014/03/04/kde-frameworks-5-alpha-two-out'>on the Dot</a>, KDE's news site.

#### Installing frameworks Alpha 2 Binary Packages

<em>Packages</em>.
A variety of distributions offers frequently updated packages of Frameworks 5. This includes Arch Linux, AOSC, Fedora, Kubuntu and openSUSE. See <a href='http://community.kde.org/Frameworks/Binary_Packages'>this wikipage</a> for an overview.

#### Compiling frameworks

The complete source code for frameworks 4.97.0 may be <a href='http://download.kde.org/unstable/frameworks/4.97.0/'>freely downloaded</a>.

#### {{< i18n "supporting-kde" >}}

{{% i18n "whatiskde" %}}
