---
date: 2021-09-02
appCount: 120
image: true
layout: gear
---
+ konsole: Fix KXmlGUI toolbars and Konsole MainWindow size, [Commit,](http://commits.kde.org/konsole/fb7f838fd3138a39aea3bcb2e91f923741587137) [#430036](https://bugs.kde.org/430036), [#439339](https://bugs.kde.org/439339), [#436471](https://bugs.kde.org/436471)
+ elisa: Fix the "Files" view, [Commit](http://commits.kde.org/elisa/618cf9b589ef9dd58e3ec3dd7450a80f10e118f8)
+ skanlite: Fix image saving when preview is not shown, [Commit,](http://commits.kde.org/skanlite/a8034a8dc2de3a0987ebf7fa57c21704e30e42e9) fixes [#440970](https://bugs.kde.org/440970)
