---
date: 2024-03-21
appCount: 180
layout: gear
---
+ dolphin: Don't hide panels after minimizing ([Commit](http://commits.kde.org/dolphin/4dc9510a2a8ceb35503cc7e81c2024774491ce8a), fixes bug [#481952](https://bugs.kde.org/481952))
+ konversation: Fix closing to the system tray ([Commit](http://commits.kde.org/konversation/1d554cb2c29e57bfd58b9aed7643dcb60fbf8659), fixes bug [#482316](https://bugs.kde.org/482316))
+ knights:  Fix crash during game over ([Commit](http://commits.kde.org/knights/a7a87731b7474bd7e993a02fd408eace3bfd949d), fixes bug [#481546](https://bugs.kde.org/481546))
