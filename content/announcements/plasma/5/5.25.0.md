---
title: "Plasma 5.25"
subtitle: "New Looks"
description: "Plasma 5.25 brings new features and concepts to the desktop environment."
date: "2022-06-14"
images:
 - /announcements/plasma/5/5.25.0/fullscreen_with_apps.png
layout: plasma-5.25
scssFiles:
- /scss/plasma-5-25.scss
authors:
- SPDX-FileCopyrightText: 2022 Niccolò Venerandi <niccolo.venerandi@kde.org>
- SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
- SPDX-FileCopyrightText: 2022 Aniqa Khokhar <aniqa.khokhar@kde.org>
- SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
SPDX-License-Identifier: CC-BY-4.0
highlights:
  title: Highlights
  items:
  - header: Gestures
    text: Gestures on touchpads and touchscreens put Plasma at your fingertips 
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Highlights_Gestures_FINAL_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Highlights_Gestures_FINAL_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Highlights_Gestures_FINAL_720p.webm
    hl_class: overview
  - header: Colors
    text: Bored of grey? Plasma puts a literal rainbow of possibilities at your disposal
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Highlights_Colors_FINAL_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Highlights_Colors_FINAL_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Highlights_Colors_FINAL_720p.webm
    hl_class: hl-krunner
  - header: Tailor-made
    text: Customizing your desktop has never been easier... or more fun!
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Highlights_Customize_FINAL_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Highlights_Customize_FINAL_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Highlights_Customize_FINAL_720p.webm
    hl_class: fingerprint
draft: false
---
{{< container class="text-center" >}}

A host of new features and cool fresh concepts in Plasma 5.25 give you a peek into the future of KDE's desktop.

{{< /container >}}

{{< highlight-grid >}}

{{< container class="text-center" >}}

## Navigate Workspaces

KDE Plasma 5.25 redesigns and enhances how you navigate between windows and workspaces.

### Overview

The **Overview effect** shows all of your open windows and virtual desktops.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Overview_FINAL_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Overview_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Overview_FINAL_720p.png" fig_class="mx-auto max-width-800" >}}

You can **search** for apps, documents, and browser tabs with *KRunner* and the *Application Launcher*.

You can add, remove, and rename virtual desktops.

{{< alert div_class="plasma-alert" color="success" icon="tip" >}}

Hold down the <kbd>Meta</kbd> ("Windows") key and press <kbd>W</kbd> to enter Overview mode or use a four-finger pinch on your trackpad.

{{< /alert >}}

### Gestures

On your **touchpad**:

{{< feature-video webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_4FingerPinch_FINAL_720p.webm" mp4="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_4FingerPinch_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_4FingerPinch_FINAL_720p.png" >}}

A *four-finger pinch* opens the Overview.

{{< /feature-video >}}

{{< feature-video webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_3FingerSwipe_FINAL_720p.webm" mp4="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_3FingerSwipe_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_3FingerSwipe_FINAL_720p.png" >}}

A *three-finger swipe* in any direction will switch between *Virtual Desktops*.

{{< /feature-video >}}

{{< feature-video webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_4FingerDownSwipe_FINAL_720p.webm" mp4="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_4FingerDownSwipe_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_4FingerDownSwipe_FINAL_720p.png" >}}

A *downwards four-finger swipe* opens *Present Windows*.

{{< /feature-video >}}

{{< feature-video webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_4FingerUpSwipe_FINAL_720p.webm" mp4="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_4FingerUpSwipe_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchPad_4FingerUpSwipe_FINAL_720p.png" >}}

A *four-finger upwards swipe* activates the *Desktop Grid*.

{{< /feature-video >}}

On your **touchscreen**:

You can configure swipes from the screen edge to open *Overview*, *Desktop Grid*, *Present Windows*, and *Show Desktop* as they directly follow your finger.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchScreen_FINALM_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchScreen_FINALM_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchScreen_FINAL_720p.png" fig_class="mx-auto max-width-800" >}}

{{< alert div_class="plasma-alert" color="success" icon="tip" >}}

Open *System Settings* and pick the open *Workspace Behavior* tab, and then *Touch Screen*. Click on any of the squares shown on the sides of the monitor icon and a dropdown will open. Select *Overview*, *Desktop Grid*, *Present Windows*, or *Desktop Grid* and click *Apply*. Now you can slide your finger from the edge of the screen you selected towards the middle of the screen and watch the magic happen.

{{< /alert >}}

{{< /container >}}

{{< twilight >}}

## Colors

**Sync the accent color with your wallpaper!** The dominant color of your background picture can be applied to all components that use the accent color.

{{< alert div_class="plasma-alert" color="success" icon="tip" >}}

Open *System Settings* and choose the *Appearance* tab, then *Colors*. Select *From current wallpaper* and click *Apply*. It is that easy.

{{< /alert >}}

With **slideshow wallpapers**, colors update when the wallpaper changes.

**Tint all colors of any color scheme using the accent color** and adapt the color of elements of every window to the  background. **You can also choose how much tint you'd like to see** mixed in with your normal color scheme.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Wallpaper_Colors_Tint_FINAL_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Wallpaper_Colors_Tint_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Wallpaper_Colors_Tint_FINAL_720p.png" fig_class="mx-auto max-width-800" >}}

{{< alert div_class="plasma-alert" color="success" icon="tip" >}}

Open *System Settings* and then click on the *Appearance* tab. Choose *Colors* and click on the *Edit* icon (the little pencil button) in the lower-right corner of a color scheme preview and a configuration dialogue will open. In the *Options* tab, tick the *Tint all colors with the accent colors* box and slide *Tint strength:* to the desired value. Click *Save as* and give your color scheme a new name. Click *Close*. Select your newly created color scheme and click *Apply*.

{{< /alert >}}

While configuring your color scheme, you can also **make the header area or titlebar use the accent color**.

{{< /twilight >}}

{{< container class="monitor" >}}

## Touch and Feel

**Activate Touch Mode** by detaching the screen, rotating it 360°, or enabling it manually.

{{< alert div_class="plasma-alert" color="success" icon="tip" >}}

If your laptop supports detaching or rotating back the keyboard, do that now. *Touch Mode* will activate. If not, you can manually enable *Touch Mode* by opening *System Settings*, clicking on the *Workspace Behavior* tab, and selecting the *Touch Mode: Always Enabled* radio button at the end of the page.

{{< /alert >}}

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchMode_FINAL_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchMode_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/TouchMode_FINAL_720p.png" fig_class="mx-auto max-width-800" >}}

*The Task Manager* and the *System Tray* **become bigger** when in *Touch Mode* making it easier on your fingers. You can customize the size of the icons when *Touch Mode* is disabled, too.

{{< alert div_class="plasma-alert" color="success" icon="tip" >}}

To manually increase icon spacing in the *Task Manager*, right-click the Task Manager and select *Configure Icons-Only Task Manager*. Select *Large* in the *Spacing between icons:* option. To manually increase the icon spacing in the *System Tray*, right-click on the *System Tray* and select *Configure System Tray*. Select *Large* in the *Panel icon spacing:* option.

{{< /alert >}}

**Titlebars of KDE apps become taller when in Touch Mode**, making it easier to press, drag, and close windows with touch. **Context menu items become taller when in Touch Mode**, giving you more space to tap the correct one.

### Customization

**Floating Panels** add a margin all around the panel to make it float, while animating it back to look normal when a window is maximized.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Floating_panel_FINAL_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Floating_panel_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Floating_panel_FINAL_720p.png" fig_class="mx-auto max-width-800" >}}

{{< alert div_class="plasma-alert" color="success" icon="tip" >}}

Right-click the panel, select *Enter Edit Mode*, and then *More Options*. Select *Floating*.

{{< /alert >}}

**Blend Effects** gracefully animate the transition when color schemes are changed.

Move your entire desktop, with folders, widgets and panels, from one monitor to another with the **Containment Management window**.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Containment_FINAL_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Containment_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Containment_FINAL_720p.png" fig_class="mx-auto max-width-800" >}}

{{< alert div_class="plasma-alert" color="success" icon="tip" >}}

Right-click on the desktop and select *Enter Edit Mode*. Choose *Manage Desktop and Panels* from the top toolbar and drag and drop desktops or panels from one display to another, or click on their hamburger menu.

{{< /alert >}}

{{< /container >}}

{{< container class="text-center" >}}

## Other Updates

* **The Global Theme settings page lets you pick and choose which parts to apply** so you can apply only the parts of a global theme that you like best.

* **The Application page for Discover has been redesigned** and gives you links to the application's documentation and website, and shows what system resources it has access to.

* If you get your password wrong, the lock and login screens shake, giving you a visual cue to try again.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Shaking_Password_FINAL_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Shaking_Password_FINAL_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Videos/Shaking_Password_FINAL_720p.png" fig_class="mx-auto max-width-800" muted="true" >}}

* **The KWin Scripts settings page has been rewritten** making it easier to manage your window manager scripts.

* **Plasma panels can now be navigated with the keyboard**, and you can assign custom shortcuts to focus individual panels.

{{< alert div_class="plasma-alert" color="success" icon="tip" >}}

Hold down the <kbd>Meta</kbd> ("Windows") and <kbd>Alt</kbd> keys, then press <kbd>P</kbd> to cycle focus between all your panels and navigate between their widgets with the arrow keys. You can also  right-click a panel and select *Enter Edit Mode*. Then choose *More Options* so you can set a custom shortcut to focus that particular panel.

{{< /alert >}}

... And there's much more going on. If you would like to see the full list of changes, [check out the changelog for Plasma 5.25](/announcements/changelogs/plasma/5/5.24.5-5.25.0).

{{< /container >}}
