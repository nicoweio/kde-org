---
custom_about: true
custom_contact: true
title: SourceForge hosts KDE CVS
date: "2000-02-10"
description: KDE and VA Linux announced at the recent Linux World Expo that the KDE development system (our CVS) will be hosted by VA Linux with their SourceForge setup. SourceForge is a Open Source project hosting site located at http://www.sourceforge.net
---

KDE and VA Linux announced at the recent Linux World Expo that the KDE
development system (our CVS) will be hosted by VA Linux with their
SourceForge setup. SourceForge is a Open Source project hosting site
located at [http://www.sourceforge.net](http://www.sourceforge.net)

This Q&A will attempt to answer some of your questions even before you
ask them!

Q: What _exactly_ will SourceForge be hosting?

A: Initially, they will be hosting only our CVS repository (currently
hosted at a university in Germany). They will also very likely
host our FTP server sometime soon if everything goes well.

<br>

Q: Is that it?

A: Well, that depends. SourceForge has a very rich set of tools at
our disposal, including mailing lists and bug tracking. We will
evaluate those tools on a case-by-case basis to see if they fit our
needs.

<br>

Q: Will the www.kde.org website be hosted by VA Linux?

A: No.

<br>

Q: Why are you switching to a new host? What was wrong with the old
one?

A: First, we are very grateful to Uni-Luebeck for hosting our CVS for
as long as they did. However, the KDE project is just getting too
big. Our server and bandwith needs are quite high and getting
higher at a continuous rate. Uni-Luebeck has been very good to us
in the past.. but it's just not fair to them (and to the students
there) to continue hogging their resources and their system
administrators. By moving to a host dedicated only to Open Source
projects, we can expand our resources and make necessary demands
without feeling like we are imposing on someone or getting in the
way.

<br>

Q: What will developers have to do to work with the new system.

A: The only big change is that the CVS now uses ssh instead of the old
unsecure pserver mechanism. That means that all developers must
have ssh installed on their development machines. All developers
with up to date entries in kde-common/accounts will be
automatically added to the SourceForge CVS. More details will be
forthcoming when they emerge.

<br>

Q: Will non-developers still have CVSup access?

A: Yes.

<br>

Q: Will I need ssh to access CVSup?

A: No. The only people that need ssh are developers with write access
to CVS. If you are not a developer, you will not need ssh.

<br>

Q: Will I need ssh to compile KDE?

A: No. Compiling KDE will never require the use of ssh. There was a
bug in the kde configure scripts that seemed to indicate this but
that was a mistake and a complete coincidence.

<br>

Q: What version of ssh is needed?

A: The last "free" version -- 1.2.27 (or something similar to that).
OpenSSH should work.

<br>

Q: Will KDE be using anonymous CVS with SourceForge?

A: At least at first. There _may_ be possible problems with
anonymous CVS locking out write access by developers. If that
happens, then anon CVS will be disabled. We'll see.

<br>

If you have any other questions, please direct them to Kurt Granroth
[granroth@kde.org](mailto:granroth@kde.org). I will answer most questions directly but willcompile a list of frequently asked ones if such a need arises.
