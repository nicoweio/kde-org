    <table border="0" cellpadding="4" cellspacing="0">
      <tr valign="top">
        <th align="left">Location</th>

        <th align="left">Size</th>

        <th align="left">MD5&nbsp;Sum</th>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/arts-1.1.4.tar.bz2">arts-1.1.4</a></td>

        <td align="right">969KB</td>

        <td><tt>aa4bef1e80cd3795e3fd832471e348e9</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kde-i18n-3.1.4.tar.bz2">kde-i18n-3.1.4</a></td>

        <td align="right">138MB</td>

        <td><tt>fe6fe3d722a12760522b688b749beca6</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdeaddons-3.1.4.tar.bz2">kdeaddons-3.1.4</a></td>

        <td align="right">1.1MB</td>

        <td><tt>1e9e3159994583a38e2451380bccdae2</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdeadmin-3.1.4.tar.bz2">kdeadmin-3.1.4</a></td>

        <td align="right">1.5MB</td>

        <td><tt>8e0d2eedc7bd785ffdd428209a6ea9b8</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdeartwork-3.1.4.tar.bz2">kdeartwork-3.1.4</a></td>

        <td align="right">14MB</td>

        <td><tt>042e42ade90a8d137edea88eb7d3e11d</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdebase-3.1.4.tar.bz2">kdebase-3.1.4</a></td>

        <td align="right">15MB</td>

        <td><tt>e6859ad85b176e11ce997490786c124d</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdeedu-3.1.4.tar.bz2">kdeedu-3.1.4</a></td>

        <td align="right">20MB</td>

        <td><tt>e710e5840b2f33e9c96b9b49d3914d5b</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdegames-3.1.4.tar.bz2">kdegames-3.1.4</a></td>

        <td align="right">8.2MB</td>

        <td><tt>be604fb91e24f990659f5cab2ac8decf</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdegraphics-3.1.4.tar.bz2">kdegraphics-3.1.4</a></td>

        <td align="right">4.4MB</td>

        <td><tt>c695c8574efca207c445dcef0bb4ae43</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdelibs-3.1.4.tar.bz2">kdelibs-3.1.4</a></td>

        <td align="right">11MB</td>

        <td><tt>82c265de78d53c7060a09c5cb1a78942</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdemultimedia-3.1.4.tar.bz2">kdemultimedia-3.1.4</a></td>

        <td align="right">5.7MB</td>

        <td><tt>f9f2b0b79a6a9ced3ae601b57da53129</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdenetwork-3.1.4.tar.bz2">kdenetwork-3.1.4</a></td>

        <td align="right">4.8MB</td>

        <td><tt>83d69ab3be7b4b10b898b250eb2d9046</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdepim-3.1.4.tar.bz2">kdepim-3.1.4</a></td>

        <td align="right">3.2MB</td>

        <td><tt>2a3e97a2cd5de688294cb39001282048</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdesdk-3.1.4.tar.bz2">kdesdk-3.1.4</a></td>

        <td align="right">2.1MB</td>

        <td><tt>ed343cedeab6c6dc814fa8b4e164e4ec</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdetoys-3.1.4.tar.bz2">kdetoys-3.1.4</a></td>

        <td align="right">1.8MB</td>

        <td><tt>58c9a31f31564513414fbaf09fc13b29</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/kdeutils-3.1.4.tar.bz2">kdeutils-3.1.4</a></td>

        <td align="right">1.4MB</td>

        <td><tt>98da1b32714e38208a3cc21efc77f627</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.4/src/quanta-3.1.4.tar.bz2">quanta-3.1.4</a></td>

        <td align="right">2.8MB</td>

        <td><tt>b5ee8c2e9d3d89d62198cfeadac8d65e</tt></td>
      </tr>
    </table>
