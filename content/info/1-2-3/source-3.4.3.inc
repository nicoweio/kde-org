<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/arts-1.4.3.tar.bz2">arts-1.4.3</a></td>
   <td align="right">922kB</td>
   <td><tt>58585969a9a33784601122c77bd15a1e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdeaccessibility-3.4.3.tar.bz2">kdeaccessibility-3.4.3</a></td>
   <td align="right">7.0MB</td>
   <td><tt>02f8ffe95f253aaab8a13ab7211494dc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdeaddons-3.4.3.tar.bz2">kdeaddons-3.4.3</a></td>
   <td align="right">1.5MB</td>
   <td><tt>e927d60e94adba69e78f82d82305f4ce</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdeadmin-3.4.3.tar.bz2">kdeadmin-3.4.3</a></td>
   <td align="right">1.4MB</td>
   <td><tt>ded1ff7ea33f6634ee342b671bbe6677</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdeartwork-3.4.3.tar.bz2">kdeartwork-3.4.3</a></td>
   <td align="right">17MB</td>
   <td><tt>a571991c6e21321177febafadb61efaa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdebase-3.4.3.tar.bz2">kdebase-3.4.3</a></td>
   <td align="right">21MB</td>
   <td><tt>7b25feba2774c077601d472dae5352c8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdebindings-3.4.3.tar.bz2">kdebindings-3.4.3</a></td>
   <td align="right">6.8MB</td>
   <td><tt>0fb123df324b4d007351c89051532dee</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdeedu-3.4.3.tar.bz2">kdeedu-3.4.3</a></td>
   <td align="right">22MB</td>
   <td><tt>9f4059beb1c3495973dd80d11bcae462</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdegames-3.4.3.tar.bz2">kdegames-3.4.3</a></td>
   <td align="right">8.9MB</td>
   <td><tt>40ae2b318b94ab1832b484d2a4d3ab83</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdegraphics-3.4.3.tar.bz2">kdegraphics-3.4.3</a></td>
   <td align="right">6.2MB</td>
   <td><tt>e2b2926301204a0f587d9e6e163c06d9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdelibs-3.4.3.tar.bz2">kdelibs-3.4.3</a></td>
   <td align="right">16MB</td>
   <td><tt>0cd7c0c8a81e5d11b91b407a4aaaf3ff</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdemultimedia-3.4.3.tar.bz2">kdemultimedia-3.4.3</a></td>
   <td align="right">5.2MB</td>
   <td><tt>49fed5afad01c322d3bc8c6a175d1898</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdenetwork-3.4.3.tar.bz2">kdenetwork-3.4.3</a></td>
   <td align="right">7.0MB</td>
   <td><tt>9c762557e6572b2d53736fefc33b028a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdepim-3.4.3.tar.bz2">kdepim-3.4.3</a></td>
   <td align="right">10MB</td>
   <td><tt>b2c145f3779578c9208dcbec9a4a5aea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdesdk-3.4.3.tar.bz2">kdesdk-3.4.3</a></td>
   <td align="right">4.3MB</td>
   <td><tt>d0d51c35769709b00b2ff7d59ed90631</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdetoys-3.4.3.tar.bz2">kdetoys-3.4.3</a></td>
   <td align="right">3.0MB</td>
   <td><tt>1dae200dc1be8527a5f7dca690cbffc1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdeutils-3.4.3.tar.bz2">kdeutils-3.4.3</a></td>
   <td align="right">2.1MB</td>
   <td><tt>d467284b523bb1268da776cd016ede4d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdevelop-3.2.3.tar.bz2">kdevelop-3.2.3</a></td>
   <td align="right">7.9MB</td>
   <td><tt>7dfae96e274c6dcb4748419452ebdf35</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4.3/src/kdewebdev-3.4.3.tar.bz2">kdewebdev-3.4.3</a></td>
   <td align="right">5.6MB</td>
   <td><tt>e3750c242404449ac9640fbf5ed7d042</tt></td>
</tr>

</table>
