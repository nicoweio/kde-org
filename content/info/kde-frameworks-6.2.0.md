---
version: "6.2.0"
title: "KDE Frameworks 6.2.0"
type: info/frameworks6
date: 2024-05-10
signer: Nicolas Fella
signing_fingerprint: 90A968ACA84537CC27B99EAF2C8DF587A6D4AAC1
custom_annc: kdes-6th-megarelease-alpha
draft: false
---
