KDE Project Security Advisory
=============================

Title:          plasma-workspace: Network access from screen locker
Risk Rating:    Low
CVE:            CVE-2015-1307
Platforms:      X11
Versions:       plasma-workspace < 5.1.95
Author:         Martin Gräßlin mgraesslin@kde.org
Date:           22 January 2015

Overview
========

Plasma's lock screen implementation uses Look and Feel packages
containing QtQuick source files to style the lock screen.
Look and Feel packages can be selected by the user using a system
settings module. A user could download a Look and Feel package and
install it locally.

The QtQuick view allows network interaction, thus a malicious Look
and Feel package could collect the user's passwords on all systems
it's installed to.

Similarly any application running under the given user could install a
different Look and Feel package to gain the user's password.

Impact
======

If a user downloaded and installed a Look and Feel package the user's
password might be sent to the author of the Look and Feel package.

Workaround
==========

Use one of the default provided Look and Feel packages.

Solution
========

For plasma-workspace upgrade to Plasma 5.1.95 or apply the following patch:
 http://commits.kde.org/plasma-workspace/0a9cea625dfcb068fb03a4deab7430b1c4ad8aa4

Credits
=======

Thanks to Martin Gräßlin for finding and fixing the issue.